<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Film szerkesztése</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="filmaddstyle.css"/>

</head>
<?php 
require_once('connect.php');
session_start();
error_reporting(0);
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null; die('<div style="color:red">Kérlek jelentkezz be!</div>');}

if($_SESSION['jog'] != "1"){
    die('<div style="color:red">Nincs megfelelő jogosultságod!</div>');
}

$id = $_GET['id'];

$result = mysqli_query($connection, "SELECT * FROM kategoriak");

if (!$result) {
    echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
    mysqli_close($connection);
    exit;
}

$result2 = mysqli_query($connection, "SELECT kategoria_id FROM film_kategoria WHERE film_id = ".$id." ORDER BY kategoria_id");
if (!$result2) {
    echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
    mysqli_close($connection);
    exit;
}


$kategoriak = array();

$index = 0;
while($row = mysqli_fetch_array($result2)){
     $kategoriak[$index] = $row['kategoria_id'];
     $index++;
}

$stmt = mysqli_prepare($connection, "SELECT id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev FROM filmek WHERE id = ?");

mysqli_stmt_bind_param($stmt, 'i', $id);
mysqli_stmt_execute($stmt);

mysqli_stmt_bind_result($stmt, $id, $cim, $leiras, $hossz, $korhatar, $kep, $megjelenes_eve, $eredeti_nyelv, $felhasznalonev);
mysqli_stmt_fetch($stmt);

mysqli_stmt_close($stmt);

$errorMessage = null;

if(isset($_POST['form_submit'])){
    $errorMessage = null;    
    $uj_cim = test_input($_POST['cim']);
    $uj_leiras = test_input($_POST['leiras']);
    $uj_hossz = test_input($_POST['hossz']);
    $uj_megjelenes = test_input($_POST['megjelenes']);
    $uj_korhatar = test_input($_POST['korhatar']);
	$uj_eredeti = test_input($_POST['eredeti']);
	$uj_kategoriak = $_POST['kategoriak'];

    if (strlen($uj_cim) == 0) {
        $errorMessage = "A cím nem lehet üres!";
    }elseif (strlen($uj_cim) >= 30) {
        $errorMessage = "A cím legfeljebb 30 karakter hosszú lehet!";
    }
	$result = mysqli_query($connection,"SELECT * FROM `filmek`");
	while($row = mysqli_fetch_array($result)){
		if ($uj_cim == $row['cim'] && $id != $row['id']) {
			$errorMessage = "A cím már létezik az adatbázisban!";
		}
	}
	
	if (!is_numeric($uj_hossz)) {
		$errorMessage = "A hossz csak szám lehet!";
	}
	
	if (!is_numeric($uj_megjelenes)) {
		$errorMessage = "A megjelenés éve csak szám lehet!";
	}

	if (!is_numeric($uj_korhatar)) {
		$errorMessage = "A korhatár csak szám lehet!";
	}

    if ($_FILES['kep']['name'] != ""){
        
         $path = $_FILES['kep']['name'];
         $extension = pathinfo($path, PATHINFO_EXTENSION);

         if($extension != "jpg" && $extension != "png" && $extension != "jpeg"
         && $extension != "gif" ) {
             $errorMessage = "Csak JPG, JPEG, PNG & GIF fájlok engedélyezettek!";
         }
    }
	
    if (!$errorMessage) {
        if ($stmt = mysqli_prepare($connection, "UPDATE filmek SET cim=?, leiras=?, hossz=?, megjelenes_eve=?, korhatar=?, eredeti_nyelv=? WHERE id=?")) {
        
            mysqli_stmt_bind_param($stmt, 'ssiiisi', $uj_cim, $uj_leiras, $uj_hossz, $uj_megjelenes, $uj_korhatar, $uj_eredeti, $id);

            if (!mysqli_stmt_execute($stmt)) {
                echo "Hiba a prepared statement végrehajtása során: " . mysqli_stmt_error($stmt);
                mysqli_close($connection);
                exit;
            }
            mysqli_stmt_close($stmt);

            if ($_FILES['kep']['name'] != ""){        

                array_map('unlink', glob("filmlist/".$id."*"));         
                
                $images = "filmlist/";
                $img = $images . $id . "." . $extension;
               
                move_uploaded_file($_FILES["kep"]["tmp_name"], $img);     
                    
			}
			
			if ($stmt = mysqli_prepare($connection, "DELETE FROM film_kategoria WHERE film_id=?")) {

				mysqli_stmt_bind_param($stmt, 'i', $id);

				if (!mysqli_stmt_execute($stmt)) {
					echo "Hiba a prepared statement végrehajtása során: " . mysqli_stmt_error($stmt);
					mysqli_close($connection);
					exit;
				}
				mysqli_stmt_close($stmt);

				foreach($uj_kategoriak as $uj_kategoria){
					if ($stmt = mysqli_prepare($connection, "INSERT INTO film_kategoria	(film_id, kategoria_id) VALUES (?, ?)")) {
						
						mysqli_stmt_bind_param($stmt, 'ii', $id, $uj_kategoria);
						mysqli_stmt_execute($stmt);
						mysqli_stmt_close($stmt);
					}
				}
			}


        } else {
            echo "Hiba a prepared statement létrehozása során: " . mysqli_error($connection);
            mysqli_close($connection);
            exit;
        }

        mysqli_close($connection);
        header('Location: ../felhaszn_felulet/filmadatlap.php?id='.$id.'');
    }

}

?>


<body class="hatter">


<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<div class="fejlec vizszkozepre">
	<a href="index.php">FilmCatalógus</a>
</div>

<div class="visszahely">
	<a class="visszagomb" href="filmadatlap.php?id=<?php echo $_GET['id']?>">Vissza</a>
</div>

<?php if(isset($errorMessage)){ ?>
<div class="error"><?php echo $errorMessage ?></div>
<?php }?>

<div class="form-body">
	<form method="post" action="" id="add-form" enctype="multipart/form-data">
	
		<div  class="kozepre">
			<h1>Film szerkesztése</h1>
			
			<div>
			<label class="label" for="cim">Cím:</label>
			<input class="input1" id="cim" type="text" name="cim" placeholder="Cím" value="<?php echo $cim; ?>" required/>
			</div>
			<div>
			<label class="label" for="leiras">Leírás:</label>
			<textarea class="input2" id="leiras" name="leiras" placeholder="Leírás" rows="30" cols="16" style="resize:vertical" required><?php echo $leiras; ?></textarea>
			</div>
			<div>
			<label class="label" for="hossz">Hossz(percben):</label>
			<input class="input3" id="hossz" type="text" name="hossz" placeholder="Hossz(percben)" value="<?php echo $hossz; ?>" required/>
			</div>
			<div>
			<label class="label" for="megjelenes">Megjelenési év:</label>
			<input class="input4" id="megjelenes" type="text" name="megjelenes" placeholder="Megjelenési év" value="<?php echo $megjelenes_eve; ?>" required/>
			</div>
			<div>
			<label class="label" for="korhatar">Korhatár:</label>
			<input class="input5" id="korhatar" type="text" name="korhatar" placeholder="Korhatár" value="<?php echo $korhatar; ?>" required/>
			</div>
			<div>
			<label class="label" for="eredeti">Eredeti nyelv:</label>
			<input class="input7" id="eredeti" type="text" name="eredeti" placeholder="Eredeti nyelv" value="<?php echo $eredeti_nyelv; ?>" required/>
			</div>
			<div>
			<label class="label" for="kategoria">Kategória:</label></br>
			<div class="katkiir">
			<?php
			if ($_POST['form_submit']){	
				$result = mysqli_query($connection, "SELECT * FROM kategoriak");
				
				if (!$result) {
					echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
					mysqli_close($connection);
					exit;
				}
			}
			$index = 1;
			while ($row = mysqli_fetch_array($result)) {
				if ($index == $kategoriak[0]){
					echo " <label class='kategolabel' for='kategoriak[]'>".$row['kategoria']."</label><input type='checkbox' name='kategoriak[]' class='kategoria' value='".$row['id']."' checked/>";
					unset($kategoriak[0]);
					$kategoriak = array_values($kategoriak);
					
				} else {
					echo " <label class='kategolabel' for='kategoriak[]'>".$row['kategoria']."</label><input type='checkbox' name='kategoriak[]' class='kategoria' value='".$row['id']."'/>";
				}
				if($index == 3){
					echo "<br/>";
				}
				$index++;
			} 
			?>       
			</div>
			</div>
			<div>
			<label class="label" for="kep">Kép:</label>
			<input class="input6" id="kep" type="file" name="kep" />
			</div>
			<div>
			<input type="submit" class="submit-gomb" name="form_submit" value="Film szerkesztése">
			</div>
		</div>
	</form>
</div>

</body>
</html>