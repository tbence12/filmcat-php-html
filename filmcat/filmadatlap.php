﻿
<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Film adatlap</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="filmadatlapstyle.css"/>

	<link type="text/css" rel="stylesheet" href="jquery.rating.css" media="screen" />

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.rating.js"></script>
	<script type="text/javascript" src="js/jquery.MetaData.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){	
			var $_GET = <?php echo json_encode($_GET); ?>;

			  $('.star-3').rating({
			cancel: 'Törlés',
			callback: function(value, link){
			  alert("Erre a filmre a leadott értkelésed: '" + value + "'");
			  
			  $.ajax({
			type:'POST',
			data:{rating:value, film_id:$_GET['id']},
			url:"rating.php", //php page URL where we post this data to save in databse
			success: function(result){
				
			
						
				
			}
		})
			
			}
		  });
		 });
	</script>
	
	
</head>

<?php require_once('connect.php');
session_start();



if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null;}

$id = $_GET['id'];
$sorszam = 1;

$stmt = mysqli_prepare($connection, "SELECT id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz FROM filmek WHERE id = ?");

mysqli_stmt_bind_param($stmt, 'i', $id);
mysqli_stmt_execute($stmt);

mysqli_stmt_bind_result($stmt, $id, $cim, $leiras, $hossz, $korhatar, $kep, $megjelenes_eve, $eredeti_nyelv, $felhasznalonev, $statusz);
mysqli_stmt_fetch($stmt);

mysqli_stmt_close($stmt);


$result = mysqli_query($connection, "SELECT id, kategoria FROM kategoriak INNER JOIN film_kategoria ON kategoriak.id = film_kategoria.kategoria_id WHERE film_id = ".$id."");

if (!$result) {
    echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
    mysqli_close($connection);
    exit;
}
$kategoria_id = array();
$index = 0;
while ($row = mysqli_fetch_array($result)){
	$kategoria_id[$index] = $row['id'];
	$index++;	
}

mysqli_data_seek( $result, 0 );
$str = implode("', '", $kategoria_id);	

	
if(!empty($kategoria_id)){
	$result2 = mysqli_query ($connection, " SELECT * 
FROM filmek INNER JOIN film_kategoria  ON filmek.id = film_kategoria.film_id
WHERE filmek.id != ".$id." AND filmek.statusz != 0 AND film_kategoria.kategoria_id IN ( '$str' ) 
ORDER BY rand() LIMIT 1
	");
	
	if (!$result2) {
		echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
		mysqli_close($connection);
		exit;
	}
	$sum = mysqli_num_rows($result2);
}
if(isset($_POST['jovahagy'])){
	if(!mysqli_query($connection, "UPDATE filmek SET statusz = 1 WHERE id = ".$id."")){
		echo "A film jóváhagyása sikertelen!";
		exit;
	} else {
		header('Location: movie_verification.php');
	}
}

$rescomm = mysqli_query($connection, "SELECT * FROM kommentek WHERE film_id = ".$id."");


$result_rating = mysqli_query($connection, "SELECT szavazat FROM szavazatok WHERE felhasznalonev = '".$_SESSION['nev']."' AND film_id = '".$id."'");

$szavazat = mysqli_fetch_array($result_rating)['szavazat'];

if(isset($_POST['submit_comment'])){

	if(!empty(test_input($_POST['comment']))){
		$result_comment = mysqli_query ($connection, "INSERT INTO kommentek (szoveg, felhasznalonev, film_id, datum) VALUES ('".$_POST['comment']."', '".$_SESSION['nev']."', '".$id."', NOW())
		");
		
		if (!$result_comment) {
			echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
			mysqli_close($connection);
			exit;
		}else{
			echo "<meta http-equiv='refresh' content='0'>";
		}
	}
	
}

mysqli_close($connection);

?>

<body class="hatter">

<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<div class="fejlec vizszkozepre">	
	<a href="index.php">FilmCatalógus</a>
</div>

<?php	 if($_SESSION['nev']){ ?>

	<div class="bejelentkezhely">
		<ul>
			<li class="dropdown">
			<a class="bejelentkezgomb" href="felhadatlap.php">Bejelentkezve, mint <?php echo $_SESSION['nev']; ?></a>
			<?php if ($_SESSION['jog'] == 1){?>
			<div class="dropdown-content">
				<a href="movie_verification.php">Filmek jóváhagyása</a>
			</div>
			<?php } ?>
			</li>
		</ul>
	</div>
<?php }else{ ?>
<div class="bejelentkezhely">
	<a class="bejelentkezgomb" href="login.php">Bejelentkezés</a>
</div>
<?php } ?>

<?php if(isset($id)){ 
	
	if (!isset($_SESSION['jog']) && $statusz == 0) {
		echo '<div class="error">A film jóváhagyásra vár! Csak admin tekintheti meg!</div>';
		exit;
	}else if(isset($_SESSION['jog']) && $_SESSION['jog'] == 0 && $statusz == 0){
		echo '<div class="error">A film jóváhagyásra vár! Csak admin tekintheti meg!</div>';
		exit;
	}
	?>




			<img src="<?php echo $kep; ?>" alt="kep" id="borito" />
			
            <article>
                <h2>
                    <?php echo $cim; ?> (<?php echo $megjelenes_eve; ?>)
                </h2>
                <div class="post-other">
					<?php echo $hossz; ?> perces, eredeti nyelve: <?php echo $eredeti_nyelv; ?>
					<br> Korhatár: <?php if($korhatar != 0) {echo $korhatar;} else {echo "Nincs";} ?>
					<br> Kategória: <?php 
					$numResults = mysqli_num_rows($result);
					$counter = 0;
					while ($row = mysqli_fetch_array($result)) { 
						if (++$counter == $numResults) {
							echo $row['kategoria'];
						} else {
							echo $row['kategoria'].', ';
						}
					} ?>
					<?php if(isset($_SESSION['jog']) && $_SESSION['jog'] == 1) {  ?>
						<br/>
					<?php if($statusz == 0){ ?>					
					<div class="icon-container">
							<form method="post" action="">
							<button type="submit" name="jovahagy" alt="jovahagyas" title="Jóváhagyás">
								<img src="source/icons8-checkmark.svg"  />
							</button>
							</form>
						</div>
					<?php } ?>
					<div class="icon-container">
						<a href="edit.php?id=<?php echo $_GET['id']; ?>">
							<img src="source/icons8-edit.svg" alt="szerkesztes" title="Szerkesztés"/>	
						</a>
					</div>	
					<div class="icon-container">
						<a href="delete.php?id=<?php echo $_GET['id']; ?>">
							<img src="source/icons8-trash.svg" alt="torles" title="Törlés"/>	
						</a>
					</div>
					<?php } ?>
				</div>
                <div class="post-content">
                    <?php echo $leiras; ?>
				</div>
				<?php if (isset($_SESSION['nev'])){?>
				<div class="csillagok">
					<form id="form" action="#" method="post">			 
					<div>
					<p> Értékeld a filmet: </p>
					<?php
						for ($i = 0; $i < 10; $i++){
							echo '<input name="example-1" type="radio" class="star-3" value="';
							echo $i+1;
							echo '"';

							if($szavazat == $i+1){
								echo 'checked';
							}

							echo '/>';
						}

					?>
					
					</div>
					<div class="clearFix"></div>
				</div>
					<?php } ?>
				<div class="visszahely">
					<a class="visszagomb" href="filmlist.php">Vissza a filmlistához</a>
				</div>
			
				<div class="hozzaszolas">
					<u>Hozzászólások</u>	
					 <?php
					 if(mysqli_num_rows($rescomm) == 0) {

					 ?>
                <div>Még nem érkezett ehhez a filmhez hozzászólás, légy te az első kommentelő!</div>
                <?php
						}
				?>
				<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				<textarea name="comment" rows="5" 	class="write_comment"></textarea>
				<input type="submit" class="comment_submit" name="submit_comment" value="Küldés">  
				</form>
				<?php	 while ($row = mysqli_fetch_array($rescomm)) { ?>
					<div class="hozzaszolkeret"><i>[<?php echo $sorszam; $sorszam = $sorszam + 1; ?>] <?php echo $row['felhasznalonev']; ?> (<?php echo $row['datum']; ?>)</i>
						<div class="hozzaszolszoveg">
							<?php echo $row['szoveg']; ?>
						</div>
					</div>
				                <?php	}	?>
				</div>
			</article>
			
			
			
			
			
			
			
			
			
			
			<?php
			if(isset($sum) &&$sum != 0){
				echo '<article id="ajanlo-content">
				<div class="ajanlo" id="ajanlo">
					<h2>Filmajánló</h2>';
					while($row = mysqli_fetch_array($result2)) { 
							
								echo "<a href='filmadatlap.php?id=".$row['id']."'>".$row['cim']." (".$row['megjelenes_eve'].")</a>";
					}
					
				echo '</div>
				</article>';
			}

		?>
			
            <div class="clearfix"></div>
		
	
<?php }else{?>

                <div class="error">Hiba, a film nem található!</div>
<?php } ?>

<script>

    setTimeout(function(){ 
		document.getElementById("ajanlo-content").style.display = "block";
		document.getElementById("borito").style.top = "33%";
	 }, 3000);

</script>
</body>
</html>