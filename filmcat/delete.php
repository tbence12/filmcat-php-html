<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Film törlése</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="filmaddstyle.css"/>

</head>
<?php 
require_once('connect.php');
session_start();
error_reporting(0);
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null; die('<div style="color:red">Kérlek jelentkezz be!</div>');}

if($_SESSION['jog'] != "1"){
    die('<div style="color:red">Nincs megfelelő jogosultságod!</div>');
}

$id = $_GET['id'];
$deleted = 0; //Törölni akarjuk-e a filmet
$exist = 0; //Létezik-e a film

$stmt = mysqli_prepare($connection, "SELECT id FROM filmek WHERE id = ?");

mysqli_stmt_bind_param($stmt, 'i', $id);
mysqli_stmt_execute($stmt);
mysqli_stmt_store_result($stmt);

if (mysqli_stmt_num_rows($stmt) != 0){
    $exist = 1;
}

mysqli_stmt_close($stmt);


if (isset($_POST['form_submit'])) {

    if ($stmt = mysqli_prepare($connection, "DELETE FROM film_kategoria WHERE film_id=?")) {

        mysqli_stmt_bind_param($stmt, 'i', $id);

        if (!mysqli_stmt_execute($stmt)) {
            echo "Hiba a prepared statement végrehajtása során: " . mysqli_stmt_error($stmt);
            mysqli_close($connection);
            exit;
        }
        mysqli_stmt_close($stmt);

        if ($stmt = mysqli_prepare($connection, "DELETE FROM filmek WHERE id=?")) {
            
            mysqli_stmt_bind_param($stmt, 'i', $id);

            if (!mysqli_stmt_execute($stmt)) {
                echo "Hiba a prepared statement végrehajtása során: " . mysqli_stmt_error($stmt);
                mysqli_close($connection);
                exit;
            }
            mysqli_stmt_close($stmt);

            if(array_map('unlink', glob("filmlist/".$id."*"))) {
                $deleted = 1; //Sikeresen törölve
            } else {
                $deleted = 2; //Hiba a törlés során
            }   
        } else {
            echo "Hiba a prepared statement létrehozása során: " . mysqli_error($connection);
            mysqli_close($connection);
            exit;
        }

    }



        mysqli_close($connection);
}



?>


<body class="hatter">


<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<div class="fejlec vizszkozepre">
	<a href="index.php">FilmCatalógus</a>
</div>

<div class="visszahely">
	<a class="visszagomb" href= <?php if($deleted != 1) {echo 'filmadatlap.php?id='.$_GET['id'];} else { echo 'filmlist.php';}?>>Vissza</a>
</div>

<div class="form-body">
	<form method="post" action="" id="add-form" enctype="multipart/form-data">
	
		<div  class="kozepre">
			<h1>Film törlése</h1>	
            <?php
            if($deleted == 1) {
            ?>
                <p>A film sikeresen törölve!</p>
                <div>
                 <input type='button' class='submit-gomb' name='back' value='Vissza a listára' onclick="window.location.href='filmlist.php';" >
                </div>
            <?php
            } else if ($deleted == 2) {
                echo "<p>Hiba a film törlése közben!</p>";
            } else if ($exist == 1){
                echo "<p>Biztosan törlöd a filmet?</p>
            <div>
                <input type='submit' class='submit-gomb' name='form_submit' value='Film törlése'>
            </div>";
            } else {
                echo "<p>Nincs ilyen film!</p>";
            }
            ?>					
		
	</form>
</div>

</body>
</html>