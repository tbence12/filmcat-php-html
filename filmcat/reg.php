<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Regisztráció</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="regstyle.css"/>

</head>
<?php 
require_once('connect.php');
session_start();
error_reporting(0);
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null;}

$errorMessage = null;
$username = test_input($_POST['username']);
$email = test_input($_POST['email']);
$pass1 = test_input($_POST['password1']);
$pass2 = test_input($_POST['password2']);

if (isset($_POST['username'])) {

    if (strlen($username) == 0) {
        $errorMessage = "A név nem lehet üres!";
    }elseif (strlen($username) >= 20) {
        $errorMessage = "A név legfeljebb 20 karakter hosszú lehet!";
    }
	$result = mysqli_query($connection,"SELECT * FROM `felhasznalok`");
	while($row = mysqli_fetch_array($result)){
		if ($username == $row['felhasznalonev']) {
			$errorMessage = "A felhasználónév foglalt";
		}
	}
	
	if( !filter_var($email, FILTER_VALIDATE_EMAIL)){
			$errorMessage = "Hibás email cím!";
	}
	
	if( strlen($pass1) < 6){
			$errorMessage = "A jelszó legalább 6 karakter hosszú legyen!";
	}
	
	if( $pass1 != $pass2){
			$errorMessage = "A jelszó nem egyezik meg!";
	}

    if (!$errorMessage) {
        if ($stmt = mysqli_prepare($connection, "INSERT INTO felhasznalok(felhasznalonev, email, jelszo) VALUES (?, ?, ?)")) {

            mysqli_stmt_bind_param($stmt, 'sss', $username, $email, sha1($pass1));

            if (!mysqli_stmt_execute($stmt)) {
                echo "Hiba a prepared statement végrehajtása során: " . mysqli_stmt_error($stmt);
                mysqli_close($connection);
                exit;
            }
            mysqli_stmt_close($stmt);
			
			header('Location: login.php?id=200');			
        } else {
            echo "Hiba a prepared statement létrehozása során: " . mysqli_error($connection);
            mysqli_close($connection);
            exit;
        }

        mysqli_close($connection);
    }
}

?>


<body class="hatter">


<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<div class="fejlec vizszkozepre">
	<a href="index.php">FilmCatalógus</a>
</div>

<div class="visszahely">
	<a class="visszagomb" href="login.php">Vissza</a>
</div>

<?php if(isset($errorMessage)){ ?>
<div class="error"><?php echo $errorMessage ?></div>
<?php }?>


<div class="form-body">
	<form method="post" action="" id="reg-form" enctype="multipart/form-data">
	
		<div  class="kozepre">
			<h1>Regisztráció</h1>
			
<?php if($_SESSION['nev']){ ?>
		<div align="center"> Már regisztrálva vagy! 
			<a href="logout.php">Kijelentkezés</a>
		</div>
<?php }else{ ?>
			
			
			<div>
			<label class="label" for="username">Felhasználónév:</label>
			<input class="input1" id="username" type="text" name="username" placeholder="Felhasználónév" required/>
			</div>
			<div>
			<label class="label" for="email">E-mail cím:</label>
			<input class="input2" id="email" type="email" name="email" placeholder="E-mail cím" required/>
			</div>
			<div>
			<label class="label" for="password1">Jelszó:</label>
			<input class="input3" id="password1" type="password" name="password1" placeholder="Jelszó" required/>
			</div>
			<div>
			<label class="label" for="password2">Jelszó ismétlés:</label>
			<input class="input4" id="password2" type="password" name="password2" placeholder="Jelszó ismétlés" required/>
			</div>			
			<div>
			<input type="submit" class="submit-gomb" name="form_submit" value="Regisztrálok">
			</div>
			<p>Ha már van fiókod, akkor </p>
			<div>
			<a href="login.php" class="reg"><u>Jelentkezz be!</u></a>
			</div>
		</div>
	

	</form>

</div>
<?php } ?>	
</body>
</html>