﻿
<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Filmek</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="filmliststyle.css"/>

</head>

<?php require_once('connect.php');
session_start();
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null;}

if(isset($_GET['search']) && isset($_GET['kategoriak'])){
    $str = $str = implode("', '", $_GET['kategoriak']);	
    $result = mysqli_query($connection, "SELECT DISTINCT id, cim, leiras, megjelenes_eve FROM filmek INNER JOIN film_kategoria ON filmek.id = film_kategoria.film_id WHERE cim LIKE '%".$_GET['search']."%' AND film_kategoria.kategoria_id IN ( '$str' ) AND statusz = 1");
} else if(isset($_GET['search'])){
    $result = mysqli_query($connection, "SELECT id, cim, leiras, megjelenes_eve FROM filmek WHERE cim LIKE '%".$_GET['search']."%' AND statusz = 1");
} else{
    $result = mysqli_query($connection, "SELECT id, cim, leiras, megjelenes_eve FROM filmek WHERE statusz = 1");        
}

if (!$result) {
    echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
    mysqli_close($connection);
    exit;
}


?>




<body class="hatter">

<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<?php require_once('header.php'); ?>

   <main>
            <?php

            if(mysqli_num_rows($result) == 0) {
                ?>
                <h1>Sajnos nincs találat!</h1>
                <?php
            }
           
            while ($row = mysqli_fetch_array($result)) {

                ?>
				<a href="filmadatlap.php?id=<?php echo $row['id']; ?>">
                <article>
                    <h2>
						<?php echo $row['cim']; ?> <span>(<?php echo $row['megjelenes_eve']; ?>) </span>
                    </h2>
                    <div class="post-content">
                        <?php echo $row['leiras']; ?>
                    </div>
                </article>
				</a>
                <?php

            }

            ?>
            <div class="clearfix"></div>
        </main>
	
</body>
</html>