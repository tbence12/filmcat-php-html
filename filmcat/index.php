<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>FilmCat</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<meta name="keywords" content="FilmCat, filmcat, Film, film, Sorozat, sorozat, Filmkatalógus, filmkatalógus, film értékelés"/>
	<link rel="stylesheet" href="filmcatstyle.css"/>

</head>
<?php require_once('connect.php');
session_start();
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null;}

?>
<body class="hatter">


<video  class="fullkozepre videomeret" autoplay loop poster="hatter2.jpg" src="honlapfilm2.mp4">
</video>


<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<?php require_once('header.php'); ?>

<div class="szovegkozepre fullkozepre">
	<p>Értékeld kedvenc filmjeid! =)</p>
	<div>
	<a  class="kozepgomb" href="filmkatego.php">Filmkategóriák</a>
	<a  class="kozepgomb" href="filmlist.php">Összes film</a>
	<a  class="kozepgomb" href="search.php">Keresés</a>
	</div>
</div>

<div class="lablec vizszkozepre">
&copy; D<sup>2</sup>B<sup>2</sup> <span id="year"></span><span>
</div>

<script>
        var d = new Date();
        var n = d.getFullYear();
        document.getElementById("year").innerHTML = n;
    </script>

</body>
</html>