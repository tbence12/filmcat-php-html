
<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Filmek</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="filmkategostyle.css"/>

</head>

<?php require_once('connect.php');
session_start();
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null;}

$result = mysqli_query($connection, "SELECT * FROM kategoriak");

if (!$result) {
    echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
    mysqli_close($connection);
    exit;
}
?>




<body class="hatter">

    <div class="logohely">
        <img class="logo" src= "logoinv.png"/>
    </div>

<?php require_once('header.php'); ?>

    <!--
    <p class="vizszkozepre cim">Filmek</p>
    -->

    <table class="tabla">
        <tr>
            <th>
            <section id="wrapper">
                <h1>Keress kedvenc filmjeid között!</h1>
                <form method="get" action="filmlist.php">
                    <div>             
                    <?php 
                    while ($row = mysqli_fetch_array($result)) {

                        echo " <input type='checkbox' name='kategoriak[]' value='".$row['id']."'>".$row['kategoria'];

                    } 
                    ?>           
                        <br/>
                        <input type="text" name="search" class="osszes" placeholder="Keresés...">
                        <button type="submit" class="osszes">Keresés</button>
                    </div>
                </form>
            </section>
            </th>
        </tr>
    </table>
</body>
</html>