<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Bejelentkezés</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="loginstyle.css"/>

</head>

<?php 
require_once('connect.php');
session_start();
error_reporting(0);
$id = $_GET['id'];
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null;}

?>


<body class="hatter">



<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<div class="fejlec vizszkozepre">	
	<a href="index.php">FilmCatalógus</a>
</div>

<div class="visszahely">
	<a class="visszagomb" href="index.php">Vissza</a>
</div>

<?php if($id == 200){ ?>
<div class="error">Sikeres regisztráció, a folytatáshoz jelentkezz be!</div>
<?php }?>


<div class="form-body">
	<div class="bejelentkez-body">

		<h1>Bejelentkezés</h1>
		
		
<?php if($_SESSION['nev']){ ?>
		<div align="center"> Már be vagy jelentkezve! 
			<a href="logout.php">Kijelentkezés</a>
		</div>
<?php }else{ ?>
	<form action="loginaction.php" method="post" id="login-form">
		<div>
			<label class="label" for="username">Felhasználónév:</label>
			<input class="input1" id="username" type="text" name="username" placeholder="Felhasználónév" required/>
		</div>
		<div>
			<label class="label" for="password1">Jelszó:</label>
			<input class="input2" id="password1" type="password" name="password1" placeholder="Jelszó" required/>
		</div>
		<div>
			<input type="submit" class="submit-gomb" name="form_submit" value="Bejelentkezek">
		</div>
	</form>
	</div>
	<p>Ha még nincs fiókod, akkor </p>
	<div>
	<a href="reg.php" class="reg"><u>Regisztrálj!</u></a>
	</div>
<?php } ?>

</div>

</body>
</html>