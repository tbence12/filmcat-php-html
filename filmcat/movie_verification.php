<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Film jóváhagyás</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="filmliststyle.css"/>

</head>
<?php
require_once('connect.php');
session_start();
if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null;}


    $result = mysqli_query($connection, "SELECT id, cim, leiras, megjelenes_eve FROM filmek WHERE statusz = 0");        


if (!$result) {
    echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
    mysqli_close($connection);
    exit;
}


?>




<body class="hatter-list">

<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<?php require_once('header.php'); ?>

   <main>
            <?php

            if(mysqli_num_rows($result) == 0) {
                ?>
                <h1>Nincs jóváhagyásra váró film!</h1>
                <?php
            }
           
            while ($row = mysqli_fetch_array($result)) {

                ?>
				<a href="filmadatlap.php?id=<?php echo $row['id']; ?>">
                <article>
                    <h2>
						<?php echo $row['cim']; ?> <span>(<?php echo $row['megjelenes_eve']; ?>) </span>
                    </h2>
                    <div class="post-content">
                        <?php echo $row['leiras']; ?>
                    </div>
                </article>
				</a>
                <?php

            }

            ?>
            <div class="clearfix"></div>
        </main>
	
</body>
</html>