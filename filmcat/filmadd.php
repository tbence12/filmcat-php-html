﻿<!DOCTYPE html>
<html lang="hu">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title>Film hozzáadás</title>
	<link rel="icon" href="logo.png" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" charset="UTF-8">
	<!--<link rel="stylesheet" href="style.css"/>-->
	<link rel="stylesheet" href="filmaddstyle.css"/>

</head>
<?php 
require_once('connect.php');
session_start();
error_reporting(0);



if (isset($_SESSION['nev'])){}
else{$_SESSION['nev'] = null; die('<div style="color:red">Kérlek jelentkezz be!</div>');}

$result = mysqli_query($connection, "SELECT * FROM kategoriak");

if (!$result) {
    echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
    mysqli_close($connection);
    exit;
}

$errorMessage = null;
$cim = test_input($_POST['cim']);
$leiras = test_input($_POST['leiras']);
$hossz = test_input($_POST['hossz']);
$megjelenes = test_input($_POST['megjelenes']);
$korhatar = test_input($_POST['korhatar']);
$eredeti = test_input($_POST['eredeti']);
$kategoriak = $_POST['kategoriak'];



if (isset($_POST['form_submit'])) {



    if (strlen($cim) == 0) {
        $errorMessage = "A cím nem lehet üres!";
    }elseif (strlen($cim) >= 30) {
        $errorMessage = "A cím legfeljebb 30 karakter hosszú lehet!";
	}
	if(!is_numeric($hossz) || !is_numeric($megjelenes) || !is_numeric($korhatar)){
		$errorMessage = "A film hossza, megjenenésének éve és korhatára csak szám lehet!";
	}
	$result = mysqli_query($connection,"SELECT * FROM `filmek`");
	while($row = mysqli_fetch_array($result)){
		if ($cim == $row['cim']) {
			$errorMessage = "A cím már létezik az adatbázisban!";
		}
	}

    if (!$errorMessage) {
        if ($stmt = mysqli_prepare($connection, "INSERT INTO filmek(cim, leiras, hossz, megjelenes_eve, korhatar, eredeti_nyelv, felhasznalonev) VALUES (?, ?, ?, ?, ?, ?, ?)")) {

            mysqli_stmt_bind_param($stmt, 'ssiisis', $cim, $leiras, $hossz, $megjelenes, $eredeti, $korhatar, $_SESSION['nev']);

            if (!mysqli_stmt_execute($stmt)) {
                echo "Hiba a prepared statement végrehajtása során: " . mysqli_stmt_error($stmt);
                mysqli_close($connection);
                exit;
            }
            mysqli_stmt_close($stmt);

            $id = mysqli_insert_id($connection);
            $extension = pathinfo($_FILES["kep"]["name"], PATHINFO_EXTENSION);

            $images = "filmlist/";
            $img = $images . $id . "." . $extension;
            if (move_uploaded_file($_FILES["kep"]["tmp_name"], $img)) {

                $stmt = mysqli_prepare($connection, "UPDATE filmek SET kep = ? WHERE id = ?");
                mysqli_stmt_bind_param($stmt, 'si', $img, $id);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }
			
			foreach($kategoriak as $kategoria){
				if ($stmt = mysqli_prepare($connection, "INSERT INTO film_kategoria	(film_id, kategoria_id) VALUES (?, ?)")) {
					
									mysqli_stmt_bind_param($stmt, 'ii', $id, $kategoria);
									mysqli_stmt_execute($stmt);
									mysqli_stmt_close($stmt);
								}
			}
			
			header('Location: filmkatego.php?id=200');	
        
		} else {
            echo "Hiba a prepared statement létrehozása során: " . mysqli_error($connection);
            mysqli_close($connection);
            exit;
        }

        mysqli_close($connection);
    }

}

?>


<body class="hatter">


<div class="logohely">
	<img class="logo" src= "logoinv.png"/>
</div>

<div class="fejlec vizszkozepre">
	<a href="index.php">FilmCatalógus</a>
</div>

<div class="visszahely">
	<a class="visszagomb" href="filmlist.php">Vissza</a>
</div>

<?php if(isset($errorMessage)){ 

	?>

<div class="error"><?php echo $errorMessage ?></div>
<?php }?>

<div class="form-body">
	<form method="post" action="" id="add-form" enctype="multipart/form-data">
	
		<div  class="kozepre">
			<h1>Film hozzáadás</h1>
			
			<div>
			<label class="label" for="cim">Cím:</label>
			<input class="input1" id="cim" type="text" name="cim" placeholder="Cím" required/>
			</div>
			<div>
			<label class="label" for="leiras">Leírás:</label>
			<textarea class="input2" id="leiras" name="leiras" placeholder="Leírás" rows="30" cols="16" style="resize:vertical" required></textarea>
			</div>
			<div>
			<label class="label" for="hossz">Hossz(percben):</label>
			<input class="input3" id="hossz" type="text" name="hossz" placeholder="Hossz(percben)" required/>
			</div>
			<div>
			<label class="label" for="megjelenes">Megjelenési év:</label>
			<input class="input4" id="megjelenes" type="text" name="megjelenes" placeholder="Megjelenési év" required/>
			</div>
			<div>
			<label class="label" for="korhatar">Korhatár:</label>
			<input class="input5" id="korhatar" type="text" name="korhatar" placeholder="Korhatár" required/>
			</div>
			<div>
			<label class="label" for="eredeti">Eredeti nyelv:</label>
			<input class="input7" id="eredeti" type="text" name="eredeti" placeholder="Eredeti nyelv" required/>
			</div>
			<div>
			<label class="label" for="kategoria">Kategória:</label></br>
			<div class="katkiir">
			<?php		
			if ($_POST['form_submit']){	
				$result = mysqli_query($connection, "SELECT * FROM kategoriak");
				
				if (!$result) {
					echo "Hiba a lekérdezés végrehajtása során: " . mysqli_error($connection);
					mysqli_close($connection);
					exit;
				}
			}
			$index = 0;
			while ($row = mysqli_fetch_array($result)) {
				echo " <label class='kategolabel' for='kategoriak[]'>".$row['kategoria']."</label><input type='checkbox' name='kategoriak[]' class='kategoria' value='".$row['id']."'/>";
				if($index == 2){
					echo "<br/>";
				}
				$index++;
			} 
			?> 
			</div>
			</div>
			<div>
			<label class="label" for="kep">Kép:</label>
			<input class="input6" id="kep" type="file" name="kep" required/>
			</div>
			<div>
			<input type="submit" class="submit-gomb" name="form_submit" value="Film hozzáadása">
			</div>
		</div>
	</form>
</div>

</body>
</html>