<?php
const DATABASE_URL = "localhost";
const DATABASE_USER = "root";
const DATABASE_PW = "";
const DATABASE_NAME = "filmcat";

$connection = @mysqli_connect(DATABASE_URL, DATABASE_USER, DATABASE_PW, DATABASE_NAME);

if (mysqli_connect_errno() == 1049) {
    $connection = @mysqli_connect(DATABASE_URL, DATABASE_USER, DATABASE_PW);
    mysqli_set_charset($connection, 'utf8'); 

	if (!mysqli_query($connection, "CREATE DATABASE IF NOT EXISTS ".DATABASE_NAME)) {
    echo "Adatbázis létrehozása sikertelen!";
    exit;
  }
	mysqli_select_db($connection, DATABASE_NAME);

if (!mysqli_query($connection, <<<EOD
CREATE TABLE `felhasznalok` (
  `felhasznalonev` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `jelszo` varchar(255) NOT NULL,
  `statusz` tinyint(1) NOT NULL DEFAULT '1',
  `jog` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A felhasznalok tábla létrehozása sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
CREATE TABLE `filmek` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cim` varchar(255) NOT NULL,
  `leiras` text NOT NULL,
  `hossz` int(11) NOT NULL,
  `korhatar` int(11) NOT NULL,
  `kep` varchar(255) NOT NULL,
  `megjelenes_eve` int(11) NOT NULL,
  `eredeti_nyelv` varchar(255) NOT NULL,
  `felhasznalonev` varchar(255) DEFAULT NULL,
  `statusz` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A filmek tábla létrehozása sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
CREATE TABLE `film_kategoria` (
  `film_id` int(11) DEFAULT NULL,
  `kategoria_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A film_kategoria tábla létrehozása sikertelen!";
    exit;
}


if (!mysqli_query($connection, <<<EOD
CREATE TABLE `film_szinesz` (
  `szinesz_id` int(11) DEFAULT NULL,
  `film_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A film_szinesz tábla létrehozása sikertelen!";
    exit;
}


if (!mysqli_query($connection, <<<EOD
CREATE TABLE `kategoriak` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `kategoria` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A kategoriak tábla létrehozása sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
CREATE TABLE `kommentek` (
  `komment_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `szoveg` text NOT NULL,
  `felhasznalonev` varchar(255) DEFAULT NULL,
  `film_id` int(11) DEFAULT NULL,
  `datum` DATE DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A kommentek tábla létrehozása sikertelen!";
    exit;
}


if (!mysqli_query($connection, <<<EOD
CREATE TABLE `szavazatok` (
  `szavazat_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `szavazat` tinyint(4) NOT NULL,
  `felhasznalonev` varchar(255) DEFAULT NULL,
  `film_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A szavazatok tábla létrehozása sikertelen!";
    exit;
}


if (!mysqli_query($connection, <<<EOD
CREATE TABLE `szineszek` (
  `szinesz_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nev` int(11) NOT NULL,
  `szuletesi_ido` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
)) {
    echo "A szineszek tábla létrehozása sikertelen!";
    exit;
}


if (!mysqli_query($connection, <<<EOD
ALTER TABLE `felhasznalok`
  ADD PRIMARY KEY (`felhasznalonev`),
  ADD UNIQUE KEY `felhasznalonev` (`felhasznalonev`,`email`);
EOD
)) {
    echo "A felhasznalok tábla alter sikertelen!";
    exit;
}


if (!mysqli_query($connection, <<<EOD
ALTER TABLE `film_kategoria`
  ADD KEY `film_id` (`film_id`,`kategoria_id`),
  ADD KEY `kategoria_id` (`kategoria_id`);
EOD
)) {
    echo "A film_kategoria tábla alter sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `film_szinesz`
  ADD KEY `szinesz_id` (`szinesz_id`,`film_id`),
  ADD KEY `film_id` (`film_id`);
EOD
)) {
    echo "A film_szinesz tábla alter sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `kommentek`
  ADD KEY `film_id` (`film_id`),
  ADD KEY `felhasznalonev` (`felhasznalonev`);
EOD
)) {
    echo "A kommentek tábla alter sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `szavazatok`
  ADD KEY `film_id` (`film_id`),
  ADD KEY `felhasznalonev` (`felhasznalonev`);
EOD
)) {
    echo "A szavazatok tábla alter sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `filmek`
  ADD CONSTRAINT `filmek_ibfk_1` FOREIGN KEY (`felhasznalonev`) REFERENCES `felhasznalok` (`felhasznalonev`) ON DELETE SET NULL ON UPDATE CASCADE;
EOD
)) {
    echo "A filmek tábla megkötése sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `film_kategoria`
  ADD CONSTRAINT `film_kategoria_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `filmek` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `film_kategoria_ibfk_2` FOREIGN KEY (`kategoria_id`) REFERENCES `kategoriak` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
EOD
)) {
    echo "A film_kategoria tábla megkötése sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `film_szinesz`
  ADD CONSTRAINT `film_szinesz_ibfk_1` FOREIGN KEY (`szinesz_id`) REFERENCES `szineszek` (`szinesz_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `film_szinesz_ibfk_2` FOREIGN KEY (`film_id`) REFERENCES `filmek` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
EOD
)) {
    echo "A film_szinesz tábla megkötése sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `kommentek`
  ADD CONSTRAINT `kommentek_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `filmek` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `kommentek_ibfk_2` FOREIGN KEY (`felhasznalonev`) REFERENCES `felhasznalok` (`felhasznalonev`) ON DELETE SET NULL ON UPDATE CASCADE;
EOD
)) {
    echo "A kommentek tábla megkötése sikertelen!";
    exit;
}

if (!mysqli_query($connection, <<<EOD
ALTER TABLE `szavazatok`
  ADD CONSTRAINT `szavazatok_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `filmek` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `szavazatok_ibfk_2` FOREIGN KEY (`felhasznalonev`) REFERENCES `felhasznalok` (`felhasznalonev`) ON DELETE SET NULL ON UPDATE CASCADE;
EOD
)) {
    echo "A szavazatok tábla megkötése sikertelen!";
    exit;
}
if (mysqli_connect_errno()) {
    echo "Hiba az adatbázishoz kapcsolódás során." . PHP_EOL;
    echo "Hibakód: " . mysqli_connect_errno() . PHP_EOL;
    echo "Hiba üzenet: " . mysqli_connect_error() . PHP_EOL;
    exit;
}



mysqli_query($connection, <<<EOD
INSERT INTO felhasznalok(felhasznalonev, email, jelszo, statusz, jog) VALUES ("admin", "admin@admin.hu", SHA1("admin"), "1", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES ("1", "A Vasember", 
"Tony Stark, a zseniális feltaláló és különc milliárdos éppen legújabb szuperfegyverét mutatja be, amikor a csoportot támadás éri és Tony mellkasába vasszilánk fúródik, mely lassan halad a szíve felé. Ráadásul foglyul ejtik és azt követelik tõle, hogy építsen meg egy minden eddiginél pusztítóbb fegyvert. Tony meg is építi, azonban egy olyan páncélöltözet formájában, amely segítségére lehet a szökésben és távol tartja a vasszilánkot a szívétõl. Így születik meg a legendás Vasember.", 
"126", "12", "filmlist/1.jpg", "2008", "angol", "admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES ("2", "Vasember 2.", 
"Kiderült, hogy a szuperhõs Vasember valójában Tony Stark. Kihasználva az iránta való érdeklõdést a milliárdos feltaláló fel akarja hívni a figyelmet a közjó szolgálatába állítható technikai újdonságokra. A kormány azonban azt szeretné, ha fejlesztéseit a hadsereg kapná. Tony azonban nem akarja megosztani a Vasemberrel kapcsolatos információit, attól fél, hogy azok rossz kezekbe kerülnek. Közben felbukkan egy rejtélyes figura a Stark család múltjából. Ivan Vanko a saját fegyverével akarja elpusztítani a Vasembert. A szuperhõs kénytelen újra akcióba lépni, hogy a barátai segítségével legyõzze azokat az erõket, amelyek õt és az emberiséget fenyegetik.", 
"124", "12", "filmlist/2.jpg", "2010", "angol", "admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES ("3", "Vasember 3.", 
"A legutóbbi küldetése után Tony Stark talonba tette a Vasembert. A tudós-szuperhõs boldogan él egykori asszisztensével, ám álmatlanságban szenved, és pánikrohamok gyötrik. Amikor merényletek sora rázza meg a várost, Stark úgy dönt, hogy felveszi a (vas)kesztyût, és szembeszáll a titokzatos ellenséggel. A Vasemberen végzett legújabb fejlesztése azonban még nem tökéletes, ráadásul felbukkan néhány korábbi ismerõse is. Az ösztönei és a találékonysága segítségével talán sikerül felülemelkedni a nehézségeken.", 
"109", "12", "filmlist/3.jpg", "2013", "angol", "admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES ("4", "Amerika Kapitány: Az elsõ bosszúálló", 
"Javában dúl a második világháború 1941-ben. A lelkes és mindenre elszánt ifjú, Steve Rogers is jelentkezik a hadseregbe, ám a gyenge fizikuma miatt kiszuperálják a sorozáson. Ezért örömmel jelentkezik a titkos katonai kísérleti programba, az Újjászületés nevet viselõ projektbe, melynek keretében szuperkatonát csinálnak belõle. Az izomkolosszussá fejlõdött Amerika kapitány feladata megállítani a nácik titkos tudománmyos részlegét, a Hydrát, melyet a világuralmi törekvéseket dédelgetõ, rettegett Vörös Koponya vezet.", 
"124", "12", "filmlist/4.jpg", "2011", "angol", "admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES ("5", "Amerika Kapitány - A tél katonája", 
"Két évvel a New Yorkért folytatott csata után Amerika kapitány, azaz Steve Rogers Washingtonban él, és próbál alkalmazkodni a modern világ kihívásaihoz. Amikor azonban a S.H.I.E.L.D. egyik munkatársát megtámadják, a világot fenyegetõ összeesküvés közepén találja magát. Szövetségesével, a Fekete Özveggyel együtt ered az igazság nyomába, miközben bérgyilkosok hada próbálja elkapni õket. Hamarosan csatlakozik hozzájuk Sólyom is, ám így is minden erejükre szükség lesz, hogy leszámoljanak ellenfelükkel.", 
"128", "16", "filmlist/5.jpg", "2014", "angol", "admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES ("6", "Amerika Kapitány: Polgárháború", 
"Az Amerika kapitány: Polgárháborúban Steve Rogers az újjáalakult Bosszúállók csapatának élén próbál gondoskodni az emberiség biztonságáról. Azonban egy újabb incidens következik be, amelynek nem várt következményei lesznek. A Bosszúállókon egyre nõ a politikai nyomás, hogy egy kormányzati szerv irányítsa õket, akiknek be kell számolniuk akcióikról. Az új rend megosztja a Bosszúállókat, a csapat két részre szakad. Az egyik csoport élén Steve Rogers áll, aki úgy véli, a Bosszúállóknak szabadon kellene tevékenykednie, hogy védelmezhessék az emberiséget. A másik frakció élén Tony Star, aki az irányítás és elszámoltathatóság eszméjét támogatja. A nézõknek döntenie kell, hogy melyik tábornak drukkolnak, miközben a háború immár két fronton zajlik.", 
"148", "16", "filmlist/6.jpg", "2016", "angol", "admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES ("7", "John Wick: 2. felvonás", 
"John Wick (Keanu Reeves), a visszavonult bérgyilkos sosem nyugszik. Miután a magányos harcos New Yorkot totális csatatérré változtatta, és bosszúhadjárata során egy sereg gyilkost iktatott ki, újra ringbe száll. Wick egykori társa, egy titokzatos nemzetközi bérgyilkos szervezet irányítását akarja megkaparintani magának, így ráveszi cimboráját, hogy ismét csatasorba álljon. Wicket kötelezi az eskü, így vonakodva, de Rómába utazik, ahol a világ legveszélyesebb gyilkosaival kell szembenéznie. És a legenda egy még durvább, még brutálisabb háborúban találja magát.", "122","18", "filmlist/7.jpg", "2017", "angol", "admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("8", 
"John Wick", 
"John Wick (Keanu Reeves) nyugodt életre vágyik. Magányosan akarja tölteni a napjait: kutyája, sportkocsija, üres, hideg lakása éppen elég neki - nincs szüksége többre. De egy nyugdíjas bérgyilkos nem pihenhet. És amikor bántják, õ sem marad tétlen. Elõveszi rég elrejtett fegyvereit, és elindul véres bosszúhadjáratára. Egyetlen ember harcol gengszterek és bérgyilkosok egész hadserege ellen, New York pedig valódi csatatérré válik. És az õrült, véres ütközetben mégsem egyértelmû, ki fog gyõzni: a gyilkosok légiója vagy a magányos harcos. Hiszen õ John Wick.",
"96",
"18", 
"filmlist/8.jpg", 
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("9",
"A feláldozhatók", 
"Barney Rossnak (Sylvester Stallone) nincs veszítenivalója. A félelmet nem ismerõ férfi a Feláldozhatók vezetõje, egy csapat zsoldosé, akik halálosak és a végsõkig is elmennek, ha egy megbízatásról van szó. Lee Christmas (Jason Statham), a pengék mestere, Yin Yang (Jet Li), a közelharc szakértõje, Gunnar Jensen (Dolph Lundgren), a mesterlövész azonnal akcióba lépnek, amikor a titokzatos Mr. Church (Bruce Willis) olyan megbízással keresi meg õket, melyet más nem vállalna el. Barney és csapata hamarosan szembekerül James Monroe-val (Eric Roberts), az egykori, dörzsölt CIA-ügynökkel. ", 
"103",
"16", 
"filmlist/9.jpg", 
"2010", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("10", 
"A feláldozhatók 2.", 
"Barney Ross (Sylvester Stallone), Lee Christmas (Jason Statham), Yin Yang (Jet Li), Gunnar Jensen (Dolph Lundgren), Dézsma (Randy Couture) és Hale Caesar (Terry Crews) újra együtt vannak, sõt két taggal, Billyvel, a Kölyökkel (Liam Hemsworth) és Maggie-vel (Yu Nan) még bõvülnek is, amikor Mr. Church (Bruce Willis) összetrombitálja a csapatot egy látszólag egyszerû melóra. Barney és régi vágású zsoldosai könnyen jövõ dohányban reménykednek, ám amikor egyiküket brutálisan meggyilkolják, a feláldozhatók bosszút esküsznek. Hiába járnak idegen területen, ahol minden ellenük dolgozik, a revánsvágytól fûtött csapat elszántan küzd, miközben az idõvel versenyt futva egy váratlan veszélyt is el kell hárítaniuk, hiszen 2,7 kg tiszta plutóniummal már a világ egyensúlyát is meg lehet borítani.",
"102",
"16", 
"filmlist/10.jpg", 
"2012", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("11", 
"A feláldozhatók 3.", 
"A Feláldozhatók visszatérnek Barney Ross-szal (Stallone) az élen, hogy eddigi legbrutálisabb ellenségükkel nézzenek szembe. Barney, a profi fõnök már megszámlálhatatlan bevetést vezetett, rengeteg szövetségest szerzett, és ellenségei számát is szépen gyarapította. Ám akivel most kell megbirkóznia a veterán zsoldosnak, arra õ sem számított. Váratlanul felbukkan Barney egykori társa, Conrad Stonebanks (Gibson) akivel annak idején együtt alapították meg a verhetetlen Feláldozhatók csapatát. A tömeggyilkossá vált egykori csapattag, a könyörtelen fegyverkereskedõ egyszer már megmenekült Barney karmai közül, és most bosszúra éhes: végleg le akar számolni egykori bandájával, célja, hogy kiiktassa a Feláldozhatókat. Ám Barneynak is megvan a maga terve. Fiatal zsoldosokkal frissíti a csapatot, akik gyorsaságukkal, technikai tudásukkal felrázzák a berozsdásodott gépezetet. És megkezdõdik az eddigi legszemélyesebb Expendables hadjárat.",
"126",
"16", 
"filmlist/11.jpg", 
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("12", 
"Amerikai bérgyilkos", 
"Mitch Rapp (Dylan O'Brien) életét egy szörnyû tragédia árnyékolja be: egy terrorista támadás során a szeme láttára ölik meg a menyasszonyát. A mindenre elszánt férfi személyes bosszút esküszik, egyetlen célja, hogy kiiktassa a kegyetlen merénylõket. Az egykori sikeres atlétára a CIA igazgatóhelyettese, Irene Kennedy (Sanaa Lathan) is felfigyel: felveszi Mitchet az ügynökség elit terroristaelhárító programjába, majd a hidegháborús veterán, a kõkemény Stan Hurley (Michael Keaton) kezeire bízza. A kiképzés során a hajthatatlan Rapp profi gyilkológéppé válik, aki nem kímél egyetlen fanatikus merénylõt sem. A fiatal hírszerzõ hamarosan a Közel-Kelet legveszélyesebb radikális harcosaival találja szembe magát, akik bármilyen eszközt képesek bevetni, hogy világuralomra törjenek.",
"112",
"18", 
"filmlist/12.jpg", 
"2017", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("13", 
"Kontroll nélkül", 
"Ava (Gina Carano) és Derek (Cam Gigandet) a Karib-szigeteken töltik nászútjukat. Egy nap Dereknek nyoma vész. Csakhamar kiderül, hogy elrabolták. Ava azonnal dönt: az emberrablókkal könyörtelenül leszámolva mindenáron kiszabadítja férjét",
"108",
"12", 
"filmlist/13.jpg", 
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("14", 
"A mestergyilkos", 
"Arthur Bishop (Jason Statham) profi bérgyilkos, aki szigorú szabályok szerint él, és mindig tiszta munkát végez a célpontok likvidálása során. A munkája professzionális tökéletességet kíván, és azt, hogy érzelmileg maximálisan függetleníteni tudja magát; Bishop pont ezen képességek miatt számít a legjobbnak szakmájában. Amikor azonban megölik Harryt (Donald Sutherland), Bishop mentorát, és egyben jó barátját, Bishop nem tudja szétválasztani a munkáját és az érzelmeit. Saját magának szabja meg a következõ feladatot: meg kell találja azokat, akik Harry haláláért felelnek. A küldetés tovább bonyolódik, amikor Harry bosszút forraló fia, Steve (Ben Foster) is feltûnik, hogy elsajátítsa Bishop szakmájának fortélyait. Bishop mindig egyedül dolgozik, de nem tudja visszautasítani Harry fiának ajánlatát. A hidegvérû bérgyilkos maga mellé veszi a lobbanékony tanítványt, és különös kapcsolat alakul ki köztük. Miközben egyre közelebb kerülnek a célpontjukhoz, a háttérben egyre komolyabb ármánykodás bontakozik ki, és a veterán problémaelhárító és új társa maguk is problémává, célpontokká válnak.",
"92",
"16", 
"filmlist/14.jpg", 
"2011", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("15", 
"A mestergyilkos 2. - Feltámadás", 
"Arthur Bishop (Jason Statham), a profi bérgyilkos, azt hitte, maga mögött tudhatja a múltat, és nyugodtan élvezheti a paradicsomi hétköznapokat gyönyörû barátnõjével, Ginával (Jessica Alba). Ám amikor egy veszélyes bûnszervezet elrabolja Ginát, Bishop kénytelen három, balesetnek álcázott gyilkosságot végrehajtani. A tiszta munkára specializálódott mestergyilkos ezúttal is a saját szabályai szerint játszik: összefog az egyik kiszemelt áldozattal (Tommy Lee Jones), hogy a segítségével együtt kiiktassák ellenségeiket, és kiszabadítsák a foglyul ejtett lányt. A hidegvérû gyilkost ezúttal semmi sem állíthatja meg. Ez az ügy sokkal személyesebb, mint eddigi bármelyik küldetése.",
"99",
"16", 
"filmlist/15.jpg", 
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("16", 
"A hét mesterlövész", 
"Calvera rablóbandája évek óta fosztogatja Mexikó falvait. A banditák idõnként lejönnek a hegyekbõl, villámgyorsan lecsapnak egy-egy településre, majd elviszik a pénzt és a termés javát. A parasztoknak mindig csak annyit hagynak, hogy élni tudjanak a következõ aratásig. Ixcatlan falu lakosai megelégelik az évente megismétlõdõ támadásokat, s egy bölcs öregtõl azt a tanácsot kapják, hogy vásároljanak lõfegyvereket a határon túl. Hilario, a farmer indul útnak és magával hoz hét mesterlövészt.",
"120",
"16", 
"filmlist/16.jpg", 
"1960", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("17", 
"A piszkos tizenkettõ", 
"Az amerikai hadsereg sokat tapasztalt katonája, Reisman õrnagy azt a feladatot kapja, hogy veszélyes bûnözõkbõl képezzen ki egy alakulatot, mely képes lesz végrehajtani minden idõk egyik legnehezebb és leglehetetlenebb kommandós akcióját. A megszállt Franciaország területén, egy szigorúan õrzött kastélyban magas rangú náci tisztek pihenik ki a háború fáradalmait. A cél az õ elpusztításuk. Ezért tizenkét halálra ítélt katona szokatlan ajánlatot kap a hadseregtõl: ha önként részt vesznek a kommandós akcióban és túlélik a bevetést, kegyelmet kapnak. Mindenki igent mond, s kezdetét veszi Reisman õrnagy és a piszkos tizenkettõ küldetése.",
"145",
"16", 
"filmlist/17.jpg", 
"1967", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("18", 
"Skyfall - 007", 
"A 007-es immár 23. alkalommal vállalkozik a lehetetlenre, hogy megmentse a világot és a brit koronát. Daniel Craig harmadszorra bújik James Bond szmokingjába. Amikor M (Judi Dench) múltjából sötét kísértet bukkan elõ, Bond hûségének is ki kell állnia a próbát. A 007-es ügynök nekivág, hogy felkutassa és elpusztítsa a fenyegetést, még akkor is, ha õ maga is súlyos árat fizet érte. Bond legutóbbi munkájába hiba csúszik, és ügynökök tucatjainak kilétére derül fény világszerte; az MI6 központját támadás éri, M-nek pedig el kell költöztetnie az egész ügynökséget. A támadások kereszttüzében álló fõnöknek egyetlen szövetségese marad: Bond. A 007-es feltûnés nélkül - csupán egy operatív ügynök, Eve (Naomie Harris) segítségével - a rejtélyes Silva (Javier Bardem) nyomába ered: reméli, hogy fényt deríthet a férfi mélyen titkolt, vészjósló indítékaira.",
"140",
"16", 
"filmlist/18.jpg", 
"2012", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("19", 
"Sherlock Holmes", 
"A legendás mesterdetektív, aki közelharcban legalább olyan verhetetlen, mint logikus gondolkodásban, élete legnagyobb problémájával kerül szembe. Sherlock Holmes (Robert Downey Jr.) és hû társa, Dr. Watson (Jude Law) Lord Blackwood (Mark Strong) nyomába vetik magukat. Ha nem leplezik le idõben, az végzetes következményekkel járhat az egész birodalomra nézve.",
"128",
"12", 
"filmlist/19.jpg", 
"2009", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("20", 
"Sherlock Holmes 2. - Árnyjáték", 
"A világ különbözõ részein különös halálesetek történnek. Senki nem veszi észre az összefüggést az események között, egyedül csak a magándetektív Sherlock Holmest ejtik gondolkodóba a történtek. Hamarosan kiderül, hogy egy zseniális, ám gátlástalan gonosztevõ, James Moriarty professzor áll az ügy háttérben. Miközben Watson doktor az esküvõjére készül, Holmest egyre váratlanabb helyzetbe sodorja nyomozása. Az ügybe nemcsak a doktort, de a saját bátyját, valamint a gyönyörû, de veszélyes jósnõt is magával rántja. Fél Európán keresztül üldözik Moriarty professzort, ám õ mindig egy lépéssel elõrébb jár.",
"129",
"12", 
"filmlist/20.jpg", 
"2011", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("21", 
"Harag", 
"1945 áprilisában, pár héttel a II. világháború befejezése elõtt a szövetségesek egyre jobban visszaszorítják az ellenséges csapatokat az európai hadszíntéren. Itt, a náci Németország kellõs közepén állomásozik Don Collier õrmester (Brad Pitt), becenevén Wardaddy és csapata. A Német Birodalom a végét járja. A katonák a végkimerültség határán állnak, az utolsó leheletükkel küzdenek az életben maradásért, és a németek által elfoglalt területek visszahódításáért. Ebben a kimerítõ, ám reményteli idõszakban csatlakozik a fiatal újonc, Norman (Logan Lerman) Wardaddy négy fõs legénységéhez, akik Sherman típusú tankjukkal vonulnak a túlerõben levõ ellenséges csapatok ellen. A kétségbeesett ifjú katona számára teljesen ismeretlen terep mind a harctér, mind pedig a Harag elnevezésû, hatalmas tank fedélzete. A harckocsi edzett, tapasztalt parancsnoka, Wardaddy szárnyai alá veszi a fiút, hogy megtanítsa neki mindazt, amivel túlélheti a háború borzalmait, és beilleszkedhet a jól összeszokott csapatba (Shia LaBeouf, Michael Pena, Jon Bernthal). Az ötfõs legénység tisztában van vele, hogy nem sok esélye maradt a több száz fõs német sereggel szemben. De bízva tankjuk védelmezõ erejében, harci tapasztalatukban és egymásba vetett hitükben, ádáz támadásba lendülnek.",
"134",
"16", 
"filmlist/21.jpg", 
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("22", 
"Ryan közlegény megmentése", 
"1944. június 6 - a normandiai partraszállás napja. Az Omaha Beach kódnevet kapott tengerparton a csónakokból védhetõ fedezékbe igyekvõ szövetséges katonák pergõtûzbe kerülnek, a vérveszteség óriási. Mindenütt halottak, sebesültek, cafatokra szakadt emberi testrészek, haldoklók halálhörgései. A pokol elsõ bugyrán éppen csak túljutott Miller százados (Tom Hanks) megmaradt embereivel újabb szinte lehetetlen próbatétel elõtt áll. Egy kis csapat élén be kell hatolnia az ellenséges hátországba, hogy felkutasson egy amerikai katonát, James Ryan közlegényt. A fõparancsnokság utasítására ugyanis azonnal le kell szerelni, mivel három bátyja a világháború különbözõ frontjain szinte egyszerre vesztette életét, s a hadsereg ezzel adózik a család tragédiájának. De a hosszú és veszélyes úton mindenkiben felmerül a kérdés: vajon miért ér ennek az egy embernek az élete többet, mint az érte küldött kilenc emberé? A sikerrendezõ Steven Spielberg 1999-ben öt Oscar-díjat nyert el a filmjével, köztük a legjobb rendezésért járót is.",
"162",
"16", 
"filmlist/22.jpg", 
"1998", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("23", 
"Django elszabadul", 
"Amerika déli része két évvel a polgárháború elõtt. Egy különös módszereket követõ profi fejvadász (Christoph Waltz) egy rabszolga (Jamie Foxx) segítségével nagy sikert ér el, és hálából felszabadítja Djangót. De a társak együtt maradnak, hogy megkeressék a fekete férfi feleségét (Kerry Washington), akit egy rabszolgapiacon láttak utoljára. A nyomok végül egy ültetvényre vezetik õket, melynek tulajdonosa (Leonardo DiCaprio) rabszolgáit - trénere segítségével - egymás elleni gladiátorküzdelemre képezi ki. A fejvadászoknak sikerül bejutniuk a birtokra, de nem biztos, hogy ki is jutnak onnan: a földesúr hû szolgája (Samuel L. Jackson) gyanút fog, és a kalandorok csapdába esnek. Dönteniük kell, hogy az önfeláldozás vagy a túlélés-e a fontosabb számukra.",
"165",
"18", 
"filmlist/23.jpg", 
"2012", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("24", 
"A fegyvertelen katona", 
"A Hacksaw Ridge Desmond Doss [Andrew Garfield] szinte hihetetlen története, aki a második világháború egyik legvéresebb csatájában Okinawa szigetén 75 bajtársa életét mentette meg úgy, hogy õ maga nem viselt fegyvert. Vallási meggyõzõdése volt, hogy noha hazája igazságos háborút vív, neki magának nem szabad ölnie. Szanitécként több alkalommal is egymaga hozta ki sebesült bajtársait az ellenséges vonalak mögül vagy a tûz alatt tartott senkiföldjérõl, miközben õ maga is súlyos sérüléseket szenvedett. Doss volt az elsõ olyan katona, aki lelkiismereti okokból megtagadta a fegyveres szolgálatot, és megkapta a Kongresszusi Becsület Érdemrendet, Amerika legmagasabb katonai kitüntetését.",
"131",
"18", 
"filmlist/24.jpg", 
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("25", 
"Elcserélt életek", 
"1928 márciusának egyik szombat reggelén, Los Angelesben Christine Collins elbúcsúzik a kilencéves kisfiától, akit egyedül nevel, azután munkába megy. Este, amikor hazaér, a legszörnyûbb rémálma válik valóra: a kis Walternek nyoma veszett. Öt hónappal késõbb a rendõrség bejelenti, hogy a kisfiú elõkerült, és a nyomozást lezárták. Holott csak rábeszélték az asszonyt, hogy vigyen haza egy kisfiút, akirõl pedig tudta, hogy nem az õ fia. Amikor Christine megpróbálja elérni a hatóságoknál, hogy folytassák a nyomozást, valóságos rágalomhadjárat indul ellene. Egyedül Briegleb tiszteletes személyében talál segítségre.",
"140",
"16", 
"filmlist/25.jpg", 
"2008", 
"angol ", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("26", 
"Halálsoron", 
"Paul Edgecomb börtönõrként szolgál a Cold Mountain fegyház siralomházában a múlt század harmincas éveiben. Az E blokkban halálraítéltek várják, hogy végig menjenek a halálsoron - a folyosón, amely a villamosszékhez vezet. Edgecomb úgy gondolja, már semmilyen meglepetés sem érheti. Ám minden megváltozik, amikor új rab érkezik az E blokkba. Az óriás termetû fekete férfit, John Coffeyt az esküdtszék két fehér gyermek meggyilkolásáért ítélte halálra. A férfi azonban egyáltalán nem tûnik gyilkosnak, sõt, egészen különös képességekkel rendelkezik. Stephen King regényébõl.",
"188",
"18", 
"filmlist/26.jpg", 
"1999", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("27", 
"A remény rabjai", 
"1946-ban egy Andy Dufresne nevû bankárt - noha makacsul hangoztatja ártatlanságát - kettõs gyilkosság elkövetése miatt életfogytiglani börtönbüntetésre ítélnek. Dufresne egy Maine állambeli büntetés-végrehajtó intézetbe kerül és hamar meg kell ismerkedjen a börtönélet kegyetlen mindennapjaival, a szadista börtönszemélyzettel, a szinte elállatiasodott rabokkal. Azonban Andy nem törik meg. A bankéletben szerzett tapasztalatai segítségével elnyeri az õrök kegyét és azzal, hogy elvállalja egyik rabtársa illegális akcióiból származó bevételeinek könyvelését, kivívja ""társai"" elismerését is. Cserébe viszont lehetõséget kap a börtön könyvtár fejlesztésére, ezzel némi emberi méltóságot csempészve a keserû körülmények között élõ rabok mindennapjaiba.",
"137",
"16", 
"filmlist/27.jpg", 
"1994", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("28", 
"Ponyvaregény", 
"Adott két idõsödõ, szabad szájú, tökös, szimpatikus gengszter, Vincent (John Travolta) és Jules (Samuel L. Jackson), akik igyekeznek fõnöküknek visszaszerezni egy aktatáskát. Ehhez persze meg kell ölniük pár embert, de ez az egyszerû bérgyilkosokkal gyakran megesik. Jules a rá célzott golyókat csodával határos módon elkerüli, s ezt jelnek tekintvén úgy dönt, felhagy eddigi életével. Társának viszont el kell vinnie szórakozni a gengszterfõnök feleségét... Van továbbá egy boxoló, Butch (Bruce Willis), aki a hírhedt marffiafõnök, Marselleus Wallace (Ving Rhames) átvágását tervezi. Hogy-hogy nem, odáig fajul a történet, hogy végül már inkább a megmentésére készül, mint a lelövésére... Nem utolsósorban, pedig, van egy piti rabló-párosunk is (Tim Roth és Amanda Plummer), akik éppen egy étterem kirablására készülnek. Ám ott reggelizik Vincent és Jules.",
"150",
"18", 
"filmlist/28.jpg", 
"1994", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("29", 
"Rekviem egy álomért", 
"Harry Goldfarb egyetlen pénzszerzési lehetõsége, hogy rendszeresen eladja anyja tévéjét, akinek a TV nézés élete egyetlen értelme és legfõbb szórakozása. A pénz heroinra kell, a TV pedig azért jó üzlet, mert Harry anyja úgyis mindig visszavásárolja. Harry édesanyja, Sara egy más világról álmodik. Mindent megtesz azért, hogy bekerülhessen kedvenc TV show-jába. Úgy érzi, ezt egyetlen úton érheti el, ha lefogy, és így fel tudja venni a ""piros ruhát"". Az egyetlen ruhát, amelyben megjelenhet a TV képernyõjén. Fanatikus fogyókúrába kezd, egy gyógyszer segítségével, melyet orvosa írt fel neki. A gyógyszer kezdetben fantasztikus hatással van Sara-ra. Fogyni kezd, feltöltõdik, teli lesz energiával és most már egészen bizonyos abban, hogy bekerül a TV show-ba. Aztán elkezdõdnek a hallucinációk... Az orvos által felírt gyógyszer gyakorlatilag ugyanolyan kemény drog, mint a heroin.",
"100",
"18", 
"filmlist/29.jpg", 
"2000", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("30", 
"Fény az óceán felett", 
"Egy ausztrál világítótorony õre Tom Sherbourne (Michael Fassbender) és felesége Isabel (Alicia Vikander) örökbe fogadnak egy tengerbõl kimentett árva kislányt. Évekkel késõbb a pár felfedezi a gyermek valódi származását, ami nem várt nehézségeket hoz életükbe.",
"133",
"12", 
"filmlist/30.jpg", 
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("31", 
"Az élet szép", 
"1938-ban járunk, amikor a szeretetreméltó, álmodozó Guido - a növekvõ rasszizmus és szûklátókörûség közepette - beleszeret visszafogott tanárnõjébe, Dorába. A csodaszép lány azonban egy helyi náci tiszt menyasszonya, Guidot pincérként látjuk viszont az eljegyzésen. Mindent elkövet, hogy a lányt bánatos jövõjétõl eltérítse... Meseszerû szerelmi történet kezdõdik.
Öt év múlva Guido és Dora házasok és született egy fiuk, Giousé is. A politikai légkör egyre romlik, végül Guidot, aki félzsidó, táborba viszik családjával együtt. Az apa, hogy túlélhesse a borzalmakat, és hogy fiát minél jobban megkímélje, azt találja ki, hogy az egész, ami körülveszi õket tulajdonképpen egy nagy játék, amelyben a rabok a versenyzõk és az õrök a játékvezetõk. Benigni az önfeláldozás, a szeretet és a védelmezés filmjét készítette el. Komikusi képességei és chaplini stílusa igen élvezetessé teszik az alkotást, amely mindazonáltal megrázóan drámai módon mutatja be a fáradhatatlan emberi lélek küzdelmeit.",
"122",
"12", 
"filmlist/31.jpg", 
"1997", 
"olasz", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("32", 
"127 óra", 
"Aron Ralston egyedül indul a Utah államban lévõ kanyon falainak megmászására. Egy rossz mozdulat következtében megcsúszik, és olyan szerencsétlenül esik, hogy egyik karja egy sziklavájatba szorul. A fiatal hegymászó öt napot tölt ebben a helyzetben, miközben lepereg elõtte az élete. Miután rájön, hogy nem számíthat segítségre, minden erejét és bátorságát összeszedve levágja a beszorult kezét. Egyedül, legyengülve a vérveszteségtõl, nem sok esélye van a menekülésre. Az akaraterejével azonban leküzdi a nehézségeket.",
"94",
"16", 
"filmlist/32.jpg", 
"2010", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("33", 
"Hatodik érzék", 
"Cole (Haley Joel Osment) hatéves, és rémálmok gyötrik. Éjszakáit és nappalait olyan lények népesítik be, akiket csak õ lát. A lények szólnak hozzá, megérintik, kérésekkel bombázzák: Cole látja a halottakat. Malcolm Crowe (Bruce Willis) gyermekpszichológus. Egy korábbi kezeltje hasba lövi, és õ az eset után más városba költözik: nehezen nyeri vissza korábbi önmagát, felejteni próbál. Cole csak benne bízik: senkinek nem beszélhet arról, amit lát, de a férfival lassan megértik egymást. Ám mégsem biztos, hogy a pszichológus meg tudja menteni a kisfiút, a halottak ugyanis egyre követelõzõbbek. Valamit akarnak a gyerektõl.",
"108",
"16", 
"filmlist/33.jpg", 
"1999", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("34", 
"Széttörve", 
"Miközben a disszociatív identitászavar mentális megosztottsága régóta lenyûgözi és próbára teszi a tudományt, él az a vélekedés is, hogy egyeseknél sajátos testi attribútumok rendelõdnek minden személyiséghez, és adott esetben fizikálisan is több személyiségre oszlik az az egyetlen illetõ.
Noha Kevin már feltárt 23 személyiséget pszichiáterének, Dr. Fletchernek, még van egy rejtett személyisége, amely felbukkan, és dominálja a többit. Miután elrabol három tinédzser lányt, köztük az önfejû és eszes Caseyt, Kevinben kitör a harc a túlélésért a többi személyiségével - és a körülötte lévõkkel - miközben elkezdnek leomlani benne a különbözõ énjeit elválasztó falak...",
"118",
"16", 
"filmlist/34.jpg", 
"2017", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("35", 
"A bárányok hallgatnak", 
"Dr. Hannibal ""Kannibál"" Lecter az egyik legveszedelmesebb pszichopata gyilkos. Évek óta szigorúan õrzött börtönben ül, de az FBI-nak most a segítségére van szüksége. Valaki ugyanis a módszereit utánozza, és remélik, hogy az õrült orvos segítségükre tud lenni. Clarice Starling különleges ügynök kapja a feladatot, hogy férkõzzön Dr. Lecter bizalmába, és próbálja meg rávenni a segítségre. Ám a dolog nem ilyen egyszerû: Dr. Lecter nem csak õrült, de hihetetlenül intelligens is, így Clarice-nek nincs könnyû dolga.",
"118",
"18", 
"filmlist/35.jpg", 
"1991", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("36", 
"Viharsziget", 
"A világtól elzárva, a Massachusetts állam partjainál fekvõ kis szigeten lévõ elmegyógyintézetben tartják fogva az Egyesült Államok legveszedelmesebb elmebeteg bûnözõit. Amikor az ötvenes években a szigorúan õrzött intézménybõl megmagyarázhatatlan módon megszökik egy gyilkosságért elítélt nõ, az igazgató, Dr. Cawley kénytelen külsõ segítséget kérni. Két rendõrbíró, Teddy Daniels és Chuck Aule utazik a szigetre, hogy kiderítse, mi történt, és megtalálja a szökevényt. Danielsnek nemcsak a munkáját hátráltató pszichiáterrel, hanem a saját démonaival is meg kell küzdenie.",
"138",
"16", 
"filmlist/36.jpg", 
"2010", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("37", 
"Holtodiglan", 
"Nick (Ben Affleck) és Amy (Rosamund Pike) ötödik házassági évfordulójukra készülnek. Viszont az ünnepi nap reggelén, Amy nyomtalanul eltûnik. Nick a rendõrséghez fordul, akik azonnal el is kezdik a nyomozást az ügyben. Ami során egyre több nyom utal arra, hogy a látszólag boldog pár élete számos sötét titkot rejt. A nyomozás elõrehaladtával szinte mindenki arra gyanakszik, hogy Nick nem is olyan ártatlan, mint amilyennek beállítja magát. Számos hazugságon érik és a helyzethez képest, eléggé furcsán viselkedik. Amivel még inkább magára tereli a rendõrség és a média figyelmét.",
"149",
"12", 
"filmlist/37.jpg", 
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("38", 
"A tökéletes trükk", 
"A bûvészek a múlt századforduló Londonjának legnagyobb sztárjai. Két fiatal varázsló van feljövõben. A feltûnõ és kifinomult Robert Angier a közönség kedvence. A kissé bárdolatlan, egyszerû Alfred Borden viszont igazi géniusz, de hiányzik belõle az a képesség, hogy lebilincselje nézõközönségét. Eleinte barátsággal és tisztelettel viseltetnek egymás iránt, de mikor a nagy mutatványuk rosszul sül el, egy életre ellenséggé válnak. Ettõl kezdve legfõbb vágyuk, hogy legyõzzék a másikat. Minden trükk és minden fellépés egyre fokozza a gyilkos versenyt közöttük.",
"130",
"16", 
"filmlist/38.jpg", 
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("39", 
"Psycho", 
"A csinos szõke titkárnõ, Marion meglép fõnöke pénzével. A városon kívül új autót vásárol, s noha ideges viselkedése feltûnik a rendõröknek, mégis tovább engedik. Éjszakára betér egy mellékút mellett meredezõ motelbe, majd a szelíd, ám különös tulajdonossal megvacsoráznak.",
"104",
"18", 
"filmlist/39.jpg", 
"1960", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("40", 
"Hannibal", 
"A világ legrémesebb gonosztevõje, Hannibal Lecter (Anthony Hopkins) megszökött és Firenzében éli a jómódúak boldog életét. Egyik áldozata, az emberi roncsként vegetáló milliomos, Mason Verger (Gary Oldman) bosszúra éhesen kutatja a vérszomjas doktort. Egy tanú felbukkanását követõen ismét remény nyílik rá, hogy elfogja Lectert, akit azonban csak egyetlen személy tud Amerikába csalogatni: Clarice Starling (Julianne Moore) FBI-ügynök. Ha sikerül õt rávenni arra, hogy újra az emberevõ pszichológus nyomába eredjen, Hannibal végre elõjön rejtekhelyérõl, hogy befejezze a halálos játékot.",
"126",
"18", 
"filmlist/40.jpg", 
"2001", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("41", 
"Memento", 
"Leonard Shelby (Guy Pearce) életét egyetlen cél vezérli: megtalálni azt, aki megölte és megerõszakolta feleségét és bosszút állni haláláért. Leonard azonban egy furcsa, gyógyíthatatlan betegségben szenved. Amnéziás és a frissen szerzett élményekre is képtelen emlékezni. Egyetlen dologban biztos csupán: bosszút akar állni. Betegsége azonban különösen megnehezíti ezt. Polaroid képekkel dokumentálja minden lépését, névjegykártyák, dokumentumok, tetoválások segítségével próbálja összerakosgatni élete és a brutális gyilkosság darabkáit. A szálak egyre bonyolódnak, mindenki gyanússá válik, talán még maga Leonard is. A nagy kérdés pedig még mindig nyitva áll: ki a gyilkos?",
"113",
"16", 
"filmlist/41.jpg", 
"2000", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("42", 
"Halálbiztos", 
"Texas legtüzesebb DJ-jének, Jungle Julia-nak az alkony leszállta azt jelzi, hogy eljött a lazulás ideje két legjobb barátnõje társaságában. A három szexi bombázó beleveti magát az éjszakába, megbolondítva és felgerjesztve a bárok törzsközönségének vágyait. Ám a feléjük irányuló figyelem közül nem mindegyik mondható ártatlannak. Valaki titokban minden lépésüket követi. Stuntman Mike, a sebhelyes arcú, cserzett bõrû férfi, gyilkos pillantásokkal fixírozza õket felturbózott autójának kormánya mögül. A lányok sörük felett ülve nem is sejtik, hogy a parkolóban, pár méterre tõlük, Mike már túráztatja halálos fegyverét, végzetes masináját.",
"90",
"18", 
"filmlist/42.jpg", 
"2007", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("43", 
"A visszatérõ", 
"A visszatérõ megtörtént események alapján készült. Magával ragadó, hátborzongató film egy férfi küzdelmérõl, a minden rettenet ellenére legyõzhetetlen élni akarásról és a teljes esélytelenséggel is dacoló emberi szellemrõl. 
Hugh Glassra (Leonardo DiCaprio) a még feltérképezetlen amerikai vadonban rátámad egy medve: társa, John Fitzgerald (Tom Hardy) szándékosan tagadja meg a segítséget és magára hagyja õt a kietlen, dermesztõ rengetegben. Glass hihetetlen erõfeszítéssel dacol a természet és az ember kegyetlenségével. Õrületes kalandokat él túl, egymaga vág keresztül hegyen, erdõn, folyón és hómezõn, hogy bosszút álljon.",
"151",
"16", 
"filmlist/43.jpg", 
"2015", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("44", 
"Vaksötét", 
"Két fiatal srác és egy lány a környékbeli házak fosztogatásával egészíti ki a zsebpénzét. Ügyesek, gyorsak, gátlástalanok, így aztán egyre sikeresebbek. Amikor kinéznek maguknak egy a világtól visszavonultan élõ vak férfit (Stephen Lang), akirõl az a hír járja, hogy milliókat rejteget otthon, azt hiszik, még az eddigieknél is könnyebb dolguk lesz.
Tévednek: az eddigieknél sokkal nehezebb feladattal találják szembe magukat: életben kell maradniuk.
A férfi ugyanis õrült. Kegyetlen, gátlástalan pszichopata, és a három betörõ fogollyá válik a házában. Attól kezdve a túlélésért küzdenek. A vak férfi azonban mintha olvasna a gondolataikban, mintha látna a sötétben, mindig elõttük jár, és mindig okosabbnak bizonyul náluk. Játszik a támadóival. Méghozzá egy nagyon kegyetlen játékot.",
"89",
"18", 
"filmlist/44.jpg", 
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("45", 
"Kill Bill", 
"A Menyasszony (Uma Thurman) egykor hírhedt bérgyilkosnõ volt, egy világklasszis nõi bérgyilkos-csapat tagja. Ám terhes lett és férjhez akart menni, de a fõnök ezt nem hagyhatta. Az esküvõje napján a csapat vezetõje, Bill (David Carradine) mindenkit lemészárol. A Menyasszony utolsó szavaival tudatja Bill-lel, hogy várandós, és az õ gyerekét hordja szíve alatt. A nõ nem hal bele a fejlövésbe, hanem ötévi kóma után véres bosszút esküszik fõnöke és egykori csapattársai ellen. Senki nem tudhatja, mikor fog következni a listán. Csak egy biztos: Bill lesz az utolsó a sorban.",
"111",
"16", 
"filmlist/45.jpg", 
"2003", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("46", 
"Alkonyattól pirkadatig", 
"Két veszedelmes bûnözõ tart a mexikói határ felé. Sokadik véres bulijukon vannak túl, amelynek során majdnem lekapcsolja õket az FBI, ezért forróvá válik a lábuk alatt a talaj. A mexikói Carlos megígéri nekik, hogy a határ túloldalán menedéket biztosít a számukra. A Gecko fivérek az éppen vakációzni készülõ Fuller tiszteletes családjának lakókocsiját veszik igénybe ennek érdekében. Megegyeznek, hogy a találkozó létrejöttével a határon túl szabadon engedik õket. A megbeszélt színhely Mexikó legvadabb kocsmája, a Titty Twister, ahol rengeteg kétes alak tanyázik. Érkezésük estéjén azonban rettenetes dolog történik és a hely igazi pokollá változik.",
"100",
"18", 
"filmlist/46.jpg",
"1996", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("47", 
"Ház a tónál", 
"Kate (Sandra Bullock) a munkája miatt megválik a gyönyörû tóparti házától. A postaládában üzenetet hagy a következõ lakónak. Alex (Keanu Reeves), az építész megdöbbenve egy elhanyagolt házat talál. Elõveszi Kate levelét és válaszol rá. Kiderül, hogy Alex és Kate nem egy idõsíkban, hanem kétévnyi távolságban élnek egymástól. Levelezni kezdenek és egy nap úgy döntenek, hogy megpróbálnak találkozni.",
"105",
"12", 
"filmlist/47.jpg",
"2006", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("48", 
"Csillagainkban a hiba", 
"A kamasz Hazel és Gus sok mindenben hasonlítanak egymásra, és még többen különböznek a kortársaiktól. A két tinédzser ugyanis a súlyos rákbetegek önsegítõ csoportjában ismerkedik meg egymással. A lány elválaszthatatlan társa egy vontatható kis oxigénpalack, a srácnak pedig már amputálni kellett a lábát. Hazel és Gus ennek ellenére teljes életet akar érni, ugyanúgy, ahogy a többi kamasz. Az egymás iránt érzett szerelmük azonban egészen új, különös utakra sodorja õket.",
"125",
"12", 
"filmlist/48.jpg",
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("49", 
"Barátság extrákkal", 
"Jamie munkaügyis fejvadász New Yorkban, Dylan nagymenõ szerkesztõ Los Angelesben. Mindketten fiatalok, sikeresek, de szerencsétlenek a szerelemben, miközben családi gondokkal küszködnek. Egy jó munka ígéretével Jamie a Nagy Almába csábítja Dylant, sõt, a munkáltató mellett maga is rákattan a srácra. Az érzés kölcsönös, ám mindketten félnek egy esetleges újabb csalódástól. A helyzet tisztázása érdekében megállapodnak, hogy a kapcsolatuk szerelemtõl és más kötöttségektõl mentes, pusztán szexuális célzatú lesz. Ám más dolog ígérni valamit, és más betartani.",
"109",
"16", 
"filmlist/49.jpg",
"2011", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("50", 
"P.S. I love you", 
"Holly boldog házasságban él Gerryvel. Amikor azonban a férje betegségben meghal, összeomlik, és nem akar tovább élni. Ám Gerry gondolt erre, a halála elõtt számos levelet írt a nejének, melyek segítségével talán úrra lehet fájdalmán, és újra megtalálhatja önmagát. Az elsõ Holly harmincadik születésnapján érkezik. A hangos levélben Gerry arra kéri, mozduljon ki otthonról és ünnepelje meg a születésnapját. Az elkövetkezõ hetekben, hónapokban újabb és újabb üzenetek jönnek a régi kedvestõl, újabb és újabb kalandra buzdítva Hollyt.",
"126",
"12", 
"filmlist/50.jpg",
"2007", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("51", 
"Angyalok városa", 
"Volt egyszer egy csodás európai film, mely sok társával egyetemben elment Amerikába ""átvedleni"" és szerencsét próbálni: Wim Wenders emlékezetes ""Berlin felett az ég"" címû filmjérõl van, szó. Ha eltekintünk a fajsúlyos különbségektõl, a maga nemében ízléses és élvezhetõ filmet kapunk. Mi történik akkor, ha egy angyal elkezd vágyódni a halandók mindennapjai után, és ki akarja próbálni a szerelmet? Nicholas Cage alakítja az angyalt, Seth-et, aki szerelmes lesz egy földi halandóba, a sebésznõ Maggie-be. Választania kell égi és földi szerelem között. Vajon mi az, ami egy racionális gondolkodású orvosnõt arra késztet, hogy egy olyan férfihoz vonzódjon, akinek a létezését is lehetetlen elfogadnia? Maggie és Seth addig harcolnak józan eszükkel és vágyaikkal, míg végül mindent feláldoznak a szerelem kedvéért.",
"117",
"12", 
"filmlist/51.jpg",
"1998", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("52", 
"A mindenség elmélete", 
"1963-ban a hírneves Cambridge-i Egyetemen Stephen Hawking, az ifjú kozmológus öles léptekkel halad a tudomány útján, és feltett szándéka, hogy talál egy ""egyszerû, meggyõzõ magyarázatot"" az univerzumra. Saját világa is kitárul, amikor fülig beleszeret egy bölcsész lányba, Jane Wilde-ba. Ám egy szerencsétlen baleset után ez az egészséges, aktív fiatalember 21 évesen megrázó diagnózissal kénytelen szembesülni: motoros neuronbetegség támadta meg végtagjait és képességeit, amely fokozatosan korlátozni fogja beszédében és mozgásában, és két éven belül bele fog halni. Jane szeretete, támogatása és elszántsága megingathatatlan, és a pár összeházasodik. Újdonsült feleségével az oldalán Stephen nem hajlandó tudomásul venni a diagnózist. Jane arra bátorítja Stephent, hogy fejezze be doktorátusát, melynek tárgya elsõ elmélete az univerzum keletkezésérõl. Családot alapítanak, és frissen szerzett és széles körben ünnepelt doktori címével felfegyverkezve Stephen belevág legambiciózusabb tudományos munkájába, melynek tárgya az, amibõl neki oly kevés adatott: az idõ. Miközben a teste egyre szûkebb korlátok közé kerül, szelleme túlszárnyal az elméleti fizika határain.",
"123",
"12", 
"filmlist/52.jpg",
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("53", 
"Büszkeség és balítélet", 
"Anglia a 18. század végén. A Bennet család életét felbolydul, amikor a közelükbe költözik Mr. Bingley, a tehetõs agglegény. A férfi baráti között ugyanis biztosan bõven akad majd kérõ mind az öt Bennet lány számára. Jane, a legidõsebb nõvér azon fáradozik, hogy meghódítsa Mr. Bingley szívét, míg a vadóc Lizzie a jóképû és dölyfös Mr. Darcyval ismerkedik meg, kirobbantva ezzel a nemek háborúját. Amikor Mr. Bingley váratlanul Londonba utazik, magára hagyva a kétségbeesett Jane-t, Lizzie Mr. Darcyt teszi felelõssé a szakításért.",
"115",
"12", 
"filmlist/53.jpg",
"2005", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("54", 
"Szerelmünk lapjai", 
"Allie és Noah tizenévesek voltak, amikor találkoztak, az elsõ pillanattól kezdve rokonszenveztek egymással. Kibontakozó szerelmük hamar beteljesedik. A lány gazdag szülei azonban ellenzik kapcsolatukat, így a két fiatal útja elválik egymástól. Amikor néhány esztendõvel késõbb újra találkoznak, a szerelmük újjáéled, és Allie-nek hamarosan választania kell társa és társadalmi rangja között. Történetüket, melynek fontos jelentõsége van számára, egy idõs úr olvassa fel újra és újra egy hasonló korú hölgynek.",
"123",
"12", 
"filmlist/54.jpg",
"2004", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("55", 
"Az 50 elsõ randi", 
"Henry a paradicsomi Hawaii-on éli az unatkozó playboyok édes életét. Egyik reggel azonban találkozik a csinos Lucyval, akinek remek a humora, bájos a mosolya és palacsintájából épp meseházat épít a tányérján. Egyszóval teljesen belehabarodik a lányba. Minden nagyszerûen alakul, de másnap kiderül, hogy Lucy semmire sem emlékszik a vidám randiból, mivel rövid távú memóriája egy autóbalesetben jóvátehetetlenül megsérült. Így Henrynek nem marad más választása: minden nap újból nekiveselkedik, hogy meghódítja Lucyt.",
"95",
"12", 
"filmlist/55.jpg",
"2004", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("56", 
"Körhinta", 
"Pataki elhatározza, hogy kilép a szövetkezetbõl, lányát Farkas Sándor egyéni gazdához adja és így egyesítik szomszédos földjeiket. Mari azonban Bíró Mátét, a szövetkezet tagját szereti. Egy vidám vásári mulatságon boldogan keringenek együtt a körhintán, amíg az apai szó vissza nem parancsolja a lányt. Egy lakodalmi mulatságon nyílt összecsapásra kerül sor a két rivális között. Mari bejelenti apjának, hogy Mátét szereti. Apja baltával támad rá, a lány elmenekül otthonról. Máté talál rá, hazaviszi és közli Patakival házassági szándékukat. Az apa kénytelen feladni a harcot.",
"90",
"12", 
"filmlist/56.jpg",
"1956", 
"magyar", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("57", 
"Benjamin Button különös élete", 
"Nem mindennapi életet él Benjamin Button (Brad Pitt). Nyolcvan esztendõsen született, az idõ múlásával pedig egyre fiatalabb lesz. 1918-ban látja meg a napvilágot New Orleansban, az elsõ világháború befejezésének pillanatában. Hosszú és különleges élete egészen a huszonegyedik századig tart. A világ számos helyén megfordul, miközben beleszeret Daisybe (Cate Blanchett), akivel az egyre növekvõ korkülönbség miatt csak néhány évet tölthet.",
"166",
"12", 
"filmlist/57.jpg",
"2008", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("58", 
"Kavarás", 
"Az elsõ randik szabálya egyszerû: tégy próbát, dönts gyorsan, ha pedig nem tetszik a másik, lépj le azonnal - és örökre. E film férfi hõse (Adam Sandler) így is tenne. Amikor elsõ randija az egyébként kedvesnek tûnõ szöszivel (Drew Barrymore) nem jön össze, elegánsan távozni készül ám a sors úgy hozza, hogy kénytelen mellette maradni. Méghozzá összezárva egy családi üdülõhelyen, korábbi házasságaikból származó gyerkõceik víg társaságában. 
A kezdeti ellenszenvbõl gyors utálat lesz. Azután sok bonyodalom következik, és közben a férfi és a nõ fokozatosan felfedezi, hogy a másik nem is olyan kiállhatatlan, mint elsõ látásra hitte.",
"117",
"12", 
"filmlist/58.jpg",
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("59", 
"Hyppolit, a lakáj", 
"Schneider Mátyás fuvarozó egyik napról a másikra feltör, gazdagságát azonban megkeseríti, hogy úrhatnám felesége egy grófi lakájt szerzõdtet, mert nagypolgári életet akar élni. Szerencsére, mert így a Kabos - Csortos kettõssel lett ez a mû a magyar hangosfilm életképességének bizonyítéka és elsõ tanújele, amelybõl nemzedékek egész sora tanulhatta meg, hogy vajon mi is az a ""magyar filmvígjáték"". Ez volt Kabos Gyula elsõ hangosfilmje, melyet Zágon István hasonló színdarabjából Nóti Károly vitt át filmre, a zenéjét pedig Eisemann Mihály szerezte.",
"75",
"12", 
"filmlist/59.jpg",
"1931", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("60", 
"Macska-jaj", 
"Grga Pitic - a cigány keresztapa és szemétdombkirály - és Zarije, egy cementüzem büszke tulajdonosa, régi jó barátok, bár vagy 25 éve nem találkoztak. A már nyolcvanas éveiket taposó, betegségekkel küszködõ két zsivány kölcsönös tiszteletben élt át együtt bûnt és bûnhõdést. Zarije fia, Matko, a semmirekellõ naplopó, mivel saját apját nem meri megkérni, Grgától kér pénzt kölcsön egy vonatrakomány benzin megszerzésére. Matko, sikeres trükkjén felbuzdulva felajánlja Dadan Karambolónak, a cigány gengszterek kábítószeres királyának, hogy beveszi a benzinbuliba. A gátlástalan Dadan azonban becsapja, így nincs más választása, mint elfogadni Dadan kárpótlási kérését, vagyis hogy hozzáadja fiát, Zarét Dadan húgához, Afroditához.",
"127",
"12", 
"filmlist/60.jpg",
"1998", 
"szerb", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("61", 
"Életrevalók", 
"Az ejtõernyõs baleset után tolószékbe kerülõ gazdag arisztokrata, Philippe felfogadja otthoni segítõnek a külvárosi gettóból jött Drisst. Azt az embert, aki most szabadult a börtönbõl, és talán a legkevésbé alkalmas a feladatra. Két világ találkozik és ismeri meg egymást, és az õrült, vicces és meghatározó közös élmények nyomán kapcsolatukból meglepetésszerûen barátság lesz, amely szinte érinthetetlenné teszi õket a külvilág számára.",
"107",
"12", 
"filmlist/61.jpg",
"2011", 
"francia", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("62", 
"Ananász expressz", 
"Dale-nek elvei vannak: 1. dolgozz minél kevesebbet. 2. szívj minél többet. 3. ne látogasd meg a csajod szüleit. De eljön az a nap, amikor az összes elvét kénytelen felrúgni. A dealere, a hozzá hasonlóan szuperlusta Saul egy fantasztikus, új anyaggal kínálja meg: az Ananász Expresszel, ami ugyan üveges tekintetet és múlhatatlan jókedvet okoz, viszont ennek köszönhetõen tanúja lesz egy gyilkosságnak, áldozata egy nyomába eredõ kábítószer-kereskedõ bandának és még a barátnõje apja is egy puskával üldözi, egyre több õrült, lehetetlen, lövöldözéssel, robbanással, bujkálással és autós száguldozással járó kaland következik. Õ csak heverészni szeret és szívni, de az Ananász Expressz száguld vele.",
"112",
"16", 
"filmlist/62.jpg",
"2008", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("63", 
"Itt a vége", 
"Filmesek sokféleképpen elképzelték már a világvégét, és nem tudjuk, melyiküknek lesz igaza. Lehet, hogy borzasztó légi csaták, vérfagyasztó hóviharok vagy kegyetlen õsállatok kísérik majd utolsó útjára a Földet. De mostantól van egy vidámabb változat is.
Egy híres hollywoodi színész, akit James Francónak hívnak (játssza: James Franco) bulit szervez. A vendéglistán természetesen csupa híresség, sztár és jól ismert partiarc szerepel. Ám a gyönyörû Los Angeles-i villa hamarosan megtelik félelemmel. A világ sajnos pont a buli közben ér látványos véget, a vendégek pedig mind másképpen reagálnak a hírnevükre fittyet hányó, feltartóztathatatlanul közelgõ katasztrófára.",
"107",
"18", 
"filmlist/63.jpg",
"2013", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("64", 
"Virsliparti", 
"Mi járhat egy virsli fejében? Vagy hát... mi járna, ha volna feje? Történetünk hõse, Frank (eredeti hangja: Seth Rogen) a lét alapkérdéseit kutatja: várja, hogy megvásárolják végre, és boldog révbe érjen... mert fogalma sincs róla, hogy a virsliket megeszik. 
Így aztán, amikor kiesik egy bevásárlókosárból, hosszú, veszélyes útra indul, hogy visszajusson a saját hûtõpolcára, mielõtt kezdõdik a nagy, július 4-i roham.
Út közben összeismerkedik pár felelõtlen virslitárssal (akik semmi másra nem vágynak, minthogy hotdogkiflikkel szexelhessenek) és másféle lényekkel: sütikkel, zöldségekkel és egyéb édes vagy keserû ételtársakkal.
A polcokon túl pedig várja õt, az étel értelme...",
"83",
"18", 
"filmlist/64.jpg",
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("65", 
"Csupasz pisztoly", 
"Az õrületes humorú Airplane, Top Secret és Nagy durranás alkotói ezúttal a ""Nagyon Különleges Ügyosztály"" aktái között bukkantak a kincsek kincsére! A fõhõs természetesen ismét Frank Drebin, a gránit állkapcsú, sziklakemény zsaru, a magasztos értékek utolsó megvesztegethetetlen védelmezõje. Miközben vadul igyekszik megakadályozni az angol királynõ ellen kifundált merényletet, ádáz bûnözõkkel és kéjsóvár asszonyokkal kell összemérnie a fegyverét. Az ügynek még részese egy szitává lõtt, de amúgy jó színben levõ ügynök, egy szõke szexbomba, két baseballcsapat és megszámlálhatatlan fura figura. Igazi ellenfele azonban csak kettõ akad: két balkeze, melyek képtelennél képtelenebb helyzetekbe sodorják.",
"81",
"12", 
"filmlist/65.jpg",
"1988", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("66", 
"Családi üzelmek", 
"A dílereknek is vannak elveik. David (Jason Sudeikis) csak rászoruló családanyáknak vagy szorgos éttermi alkalmazottaknak árul, de gyerekeknek soha. És bízik benne, hogy ha mindig kispályás marad, nem érheti baj. De persze téved. Méghozzá nagyon nagyot. Egyszer, egyetlen egyszer néhány környékbeli kamasznak segít megszabadulni az elvonási tünetektõl, azok erre kirabolják, és õ nem tud elszámolni sem az anyaggal, sem a pénzzel. Hogy jóvá tegye a hibáját kénytelen egy nagy szállítmányt áthozni Mexikóból. Szerencsére a jó szomszédja, egy cinikus sztriptíztáncosnõ (Jennifer Aniston) és két züllött kiskorú (Emma Roberts, Will Poulter) segít neki: úgy tesznek, mintha családi túrán járnának, de a lakókocsi csempészáruval van tele. Az ötlet jó - a kivitelezés viszont elképesztõ következményekkel jár... ",
"108",
"16", 
"filmlist/66.jpg",
"2013", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("67", 
"Nõi szervek", 
"Sarah Asburn és Shannon Mullins egyaránt kemény, a munkájának élõ, magányos nõ, és mindketten egyformán gorombák. Emiatt aztán ki nem állhatják egymást. A helyzeten tovább ront, hogy Ashburn nagyképû FBI-ügynök, Mullins viszont egyszerû, mocskosszájú bostoni rendõrnõ. Amikor Asburn ki akarja venni kezébõl az ügyét, rá kell jönnie, hogy emberére akadt. Így kénytelen-kelletten összefognak, hogy lekapcsolják az agafúrt és könyörtelen drogbárót. A kényszerû közös munka során megtanulják tisztelni egymást.",
"117",
"16", 
"filmlist/67.jpg",
"2013", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("68", 
"Nagyfater elszabadul", 
"Sosem tudhatod a nagyfater milyen extra igényekkel áll elõ.
Jason Kellyt (Zac Efron) egy hét választja el, hogy feleségül vegye fõnöke irányításmániás lányát, és így végre társtulajdonossá váljon apósa ügyvédi irodájába, amivel megkezdõdhet kiszámítható, tökéletesnek tûnõ élete. Azonban ez a szépen megtervezett jövõ egy csapásra veszélybe kerül, mikor Jason szabadszájú nagyapja, Dick (Robert De Niro) csõbe húzza a mit sem sejtõ ifjú titánt. Dick a felesége halálát követõ napon arra kéri unokáját utazzon el vele Daytonába, ahová a feleségével jártak el minden évben, és ahol éppen a Tavaszi Szünet nevû, vad partijairól híres fesztivál zajlik.",
"102",
"16", 
"filmlist/68.jpg",
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("69", 
"Ünneprontók ünnepe", 
"A két szélhámos jó barát, John (Owen Wilson) és Jeremy (Vince Vaughn) munkaidõben válóperre készülõ házastársakat békítenek, szabadidejükben pedig esküvõkre járnak, hogy nõt fogjanak. A lányok tapadnak, a pezsgõ isteni, és sosem kell a második randira készülni. De aki a tûzzel játszik, az megégeti magát. Márpedig Johnt elönti a forróság, amikor Cleary szenátor lányának az esküvõjén megpillantja álmai nõjét. Claire (Rachel McAdams) szintén a szenátor lánya, és hamarosan az õ fejét is beköti Zach Lodge, a nagyvárosi tapló. Nincs mese, a nászt bármi áron is, de meg kell akadályozni.",
"119",
"12", 
"filmlist/69.jpg",
"2005", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("70", 
"Baywatch", 
"A nagysikerû tévésorozat moziverziójában az elkötelezett vízimentõ, Mitch Buchanon (Dwayne Johnson) akasztja össze a bajuszt egy pimasz újonccal (Zac Efron). Ám a két vetélytárs összefog, amikor arról van szó, hogy fel kell göngyölíteniük egy bûnügyet, amely az egész öböl jövõjét veszélyezteti. 
Feszes szituk és feszes izmok, dagadó hullámok és dagadó keblek, hatalmas poénok és parányi bikinik.",
"116",
"16", 
"filmlist/70.jpg",
"2017", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("71", 
"Mike és Dave esküvõhöz csajt keres", 
"Mike (Zac Efron) és Dave (Adam DeVine) igazi sportember: õk a felelõtlen bulizás, az elhajlás, a komolytalanság és fel nem növés bajnokai. A sok futó kapcsolatnak csak egy hátránya van: amikor nõvérük férjhez megy, fogalmuk sincs, kit vigyenek magukkal a hawaii lakodalomba. Így aztán, feladnak egy hirdetést, ami viszont valahogyan önálló életre kel: pillanatok alatt százezrek látják és lájkolják a szöveget.
A sok jelentkezõ között a srácok végre rátalálnak az igazira: arra a két csajra, aki felelõtlen bulizásban, elhajlásban és komolytalanságban is túltesz rajtuk.",
"98",
"16", 
"filmlist/71.jpg",
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("72", 
"21 Jump Street - A kopasz osztag", 
"Jenko (Channing Tatum) és Schmidt (Jonah Hill) csak nemrég fejezték be a rendõrakadémiát, és nem állnak a legjobb zsaruk hírében. Közösen elbaltázott ügyük után mindössze egy esélyt kapnak. Vissza kell ülniük a padba, hogy a gimnazistákat veszélyeztetõ drogkereskedõ bandát leleplezzék. A baj csak az, hogy a gimisek már egyáltalán nem olyanok, mint õk voltak néhány évvel ezelõtt. Ezúttal Schmidtnek jut a népszerûség, míg megdöbbenésére Jenko a kockafejû lúzerek közt találja magát. A háború kezdetét veszi a suli folyosóin.",
"110",
"16", 
"filmlist/72.jpg",
"2012", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("73", 
"Star Wars: Baljós árnyak", 
"Nyugtalanság uralkodik a Galaktikus Köztársaságban. A távoli csillagrendszerekbe irányuló kereskedelmi utak megadóztatásának tervét heves viták övezik. A kapzsi Kereskedelmi Szövetség felfegyverzett csatahajóival blokád alá veszi a parányi Naboo bolygót. A Fõkancellár titokban útnak indít két Jedi lovagot, a béke és az igazság szerzõit, hogy oldják meg a konfliktust és védjék meg Amidala királynõt. Hamarosan azonban kényszerleszállást kell végrehajtaniuk a Tatooine-on, ahol találkoznak az ifjú Anakinnal, akiben túlárad az erõ.",
"136",
"12", 
"filmlist/73.jpg",
"1999", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("74", 
"Pillangó-hatás", 
"A pillangó-effektus elmélet szerint, ha egy pillangó megrebbenti a szárnyát Pekingben, az akár tornádót is gerjeszthet Amerikában. Világunkban minden mindennel összefügg, minden cselekedetünk alapjában változtathatja meg a jövõt. Még elképzelni is rémes, hogy mi lenne, ha valaki képes lenne a múltunkat megváltoztatni - ez kiszámíthatatlan következménnyel járna jelenünkre nézve. Evan Treborn gyerekkorában számtalanszor elvesztette az öntudatát, élete egyes szakaszai sötét foltok csupán az emlékezetében. Amikor tudatánál volt, naplót vezetett és most, hogy felnõttként megpróbál visszaemlékezni a múltra, a naplója segít neki ebben. Miközben Evan lassan feltérképezi saját múltját, rájön, hogy egy különös adottság birtokában van: képes az idõutazásra! Így visszatér a múltba, hogy helyrehozzon néhány baklövést, amelyek zsákutcába juttatták szeretett barátait. Ám amikor visszatér a jelenbe, rájön, hogy a múlt apró változásai nem javítottak a helyzeten, hanem még nagyobb problémákat okoztak a jelenben.",
"113",
"16", 
"filmlist/74.jpg",
"2004", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("75", 
"Egyenesen át", 
"Néhány orvosegyetemista bátor és õrült kísérletre szánja el magát. A halál utáni élet érdekli õket, hát próbaképpen meghalnak. Mindez persze laboratóriumi körülmények között zajlik, a barátaik meg ott vannak mellettük, és a megfelelõ pillanatban mindig újraélesztik õket. 
De lassacskán rá kell jönniük, hogy a bátorságuk vakmerõség volt. Átjutottak a túloldalra, és sikerült visszajönniük, de valami olyasmit hoztak magukkal, amire nem készültek fel. A halál ott marad velük, és hiába élnek, nem tudnak megszabadulni tõle...",
"108",
"16", 
"filmlist/75.jpg",
"2017", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("76", 
"A galaxis õrzõi", 
"A kalandor Peter Quill ellopja a titokzatos gömböt, amelyre a bolygóközi uralomra törõ Ronan is pályázik. A fõgonosz ezért hajtóvadászatot indít utána. Menekülés közben Quill kényszerû szövetséget köt négy különös alakkal: Rockettel, Groottal, Gamorával és Draxszal. Az állig felfegyverzett mosómedve, az élõ fára emlékeztetõ lény, a veszedelmes bérgyilkosnõ és a Pusztító alkotják a Galaxis õrzõit. Quill rájön, hogy a titokzatos gömbnek milyen ereje van, és mekkora veszélyt jelent az univerzumra nézve.",
"122",
"12", 
"filmlist/76.jpg",
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("77", 
"Csillagok között", 
"A legendásan titkolózó rendezõ új sci-fijérõl annyit már tudni, hogy csupa sztár mûködik közre benne és a csillagok között játszódik. Tudósok felfedeznek egy féreglyukat az ûrben, és egy csapatnyi felfedezõ meg kalandor nekivág, hogy átlépje mindazokat a határokat, amelyeket addig áthághatatlannak hittünk: túl akarnak lépni téren és idõn. ",
"169",
"12", 
"filmlist/77.jpg",
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("78", 
"Mentõexpedíció", 
"A Marson hatalmas homokvihar készül. A NASA ûrhajósai biztonsági okokból azonnal elhagyják a bolygót: halott társuk testét sincs már idejük megkeresni. 
Nem is találnák. Mark Watney (Matt Damon) ugyanis nem halt meg; csak a szkafanderébe épített adó meghibásodása miatt hiszik a többiek, hogy nem élte túl az elsõ szélrohamokat. 
A mérnök-botanikus mindent elkövet a túlélése érdekében: gépeket szerkeszt, élelmiszert termel, vizet gyárt, és amikor a Földi irányítók felfedezik, hogy él, felkészül a mentõexpedíció fogadására.
De az expedíció négy év múlva érkezik, méghozzá az ellenséges bolygó egy egészen másik pontjára. Neki 3200 kilométert kell utazni, hogy fogadhassa õket: egyedül, megfelelõ felszerelés nélkül, végtelen magányban.",
"136",
"12", 
"filmlist/78.jpg",
"2015", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("79", 
"Lopott idõ", 
"A nem is olyan távoli jövõben a születésekor mindenkinek a karjába beépítenek egy órát. Ahhoz, hogy valaki huszonöt éves koron túl is éljen, idõre van szüksége. A gazdagok megvásárolják az idõt, míg a gettóban kétségbeesett harc folyik a minden egyes percért. A gettólakó Will Salas megmenti egy idõmilliomos életét, aki cserébe rengeteg idõt ad neki. Mindez felkelti az Idõrendészek figyelmét, és gyilkossággal gyanúsítják. Will menekülés közben túszul ejti a fiatal, gazdag lányt. Egyik percrõl a másikra élnek, és egymásba szeretnek.",
"101",
"12", 
"filmlist/79.jpg",
"2011", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("80", 
"Képlet", 
"Egy iskolai avatóünnepségén elteszik a gyerekek jövõrõl készült rajzait egy idõkapszulába, köztük azt is, amelyik véletlenszerûnek tûnõ számsorokat tartalmaz. A kislány elmondása szerint láthatatlan emberek suttogták neki a számokat. Ötven évvel késõbb a kislány által írt számok Caleb Koestlerhez (Chandler Canterbury) kerülnek, aki megmutatja azt az apjának, a híres asztrofizikusnak. John Koestler (Nicolas Cage) megdöbbenve fedezi fel, hogy a számok dátumokat jelentenek, amelyek a világot sújtó nagy katasztrófák idõpontjait jelzik az elmúlt ötven évben. A számsort összeállító kislány lánya, Diana (Rose Byrne) segítségével John Koestler versenyt fut az idõvel, hogy megakadályozza a végsõ katasztrófát, az apokalipszist, ami elõre elrendeltetett. Alex Proyas filmje.",
"121",
"16", 
"filmlist/80.jpg",
"2009", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("81", 
"Avatar", 
"Egy deréktól lefelé megbénult háborús veterán a távoli Pandorára utazik. A bolygó lakói, a Na'vik az emberhez hasonló faj - de nyelvük és kultúrájuk felfoghatatlanul különbözik a miénktõl. Ebben a gyönyörû és halálos veszélyeket rejtõ világban a földieknek nagyon nagy árat kell fizetniük a túlélésért. De nagyon nagy lehetõséghez is jutnak: a régi agyuk megõrzésével új testet ölthetnek, és az új testben, egy idegen lény szemével figyelhetik magukat és a körülöttük lévõ felfoghatatlan, vad világot. A veterán azonban más céllal érkezett. Az új test új, titkos feladatot is jelent számára, amit mindenáron végre kell hajtania.",
"166",
"16", 
"filmlist/81.jpg",
"2009", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("82", 
"Az útvesztõ", 
"Thomas egy liftben ébred, és a nevén kívül nem emlékszik semmire. Az ajtón túl különös világ várja. 60 srác, akik elzártan, szigetszerû univerzumukban élnek, és egyetlen dolgot tanulnak: a túlélés trükkjeit. Õk nem lepõdnek meg az újonnan érkezõn: havonta egyszer mindig jön valaki. Világukat útvesztõ veszi körül. Aki szökni próbál - és sokan vannak ilyenek - mind ottvesznek. Ezek a szabályok, melyek nem változnak és nem változtathatók. Valami mégis átalakul. Egy titokzatos üzenet hatására néhány srác azt hiszi, van remény a lázadásra. Még akkor is, ha fogalmuk sincs, ki ellen kell lázadniuk, és mekkora veszéllyel próbálnak szembenézni.",
"113",
"16", 
"filmlist/82.jpg",
"2014", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("83", 
"Utazók", 
"Csodálatos a jövõ. Az Avalon nevû ûrhajó 5259 utassal a fedélzetén 120 éven át siklik elegánsan távoli célja felé, ahol az érkezõk majd új életet kezdhetnek. Addig pedig a vele utazók biztonságosan lefagyasztva alusszák kiro-álmukat, és új otthonukban frissen, fiatalon ébredhetnek fel. 
Elvileg.
A programba hiba csúszik, kiolvaszt két hibernációs ágyat, és két embert felébreszt. Jim (Chris Pratt) és Aurora (Jennifer Lawrence) maga sem tudja, miért és hogyan, egy hatalmas, néma ûrhajó fedélzetén találja magát - és még 90 év van hátra, míg elérik úti céljukat. 
Olyanok, mint két hajótörött, a lakatlan szigeten. De segítségben nem is reménykedhetnek. Saját erejükbõl próbálják megoldani a helyzetet, ám egyre újabb nehézségekbe és rejtélyekbe ütköznek. 
Ha életben akarnak maradni, meg kell érteniük, mi történt velük...",
"116",
"12", 
"filmlist/83.jpg",
"2016", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("84", 
"Forráskód", 
"Bombamerényletet követnek el egy vonat ellen. A titkosszolgálat embere, Colter Stevens százados Sean Fentress álnéven ott volt a vonaton. Feladata, hogy azonosítsa a merénylõt, aki újabb akcióra készül. A Forráskód nevû szupertitkos program segítségével Stevens-Fentresst újra és újra visszaküldik a szerelvényre, hogy megtalálja a merénylõt. Minden alkalommal újabb bizonyítékra bukkan, ám a merénylõnek sikerül elmenekülnie. Minél többet tud meg róla, Stevens annál biztosabb abban, hogy meg tudná akadályozni a robbanást, ha egyszer nem futna ki az idõbõl.",
"93",
"12", 
"filmlist/84.jpg",
"2011", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("85", 
"Páncélba zárt szellem", 
"A Páncélba zárt szellem fõhõse az Õrnagy, a különleges erõknél szolgáló egyedi humán-kiborg hibrid, a 9-es Részleg nevû kommandós osztag vezetõje. A legveszélyesebb bûnözõk és szélsõségesek kiiktatására szakosodott 9-es Részleg egy olyan ellenséggel kerül szembe, amelynek egyetlen célja, hogy elpusztítsa a HankaRobotika kibertechnológia fejlesztéseit.",
"107",
"12", 
"filmlist/85.jpg",
"2017", 
"angol", 
"admin", "1")
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO filmek(id, cim, leiras, hossz, korhatar, kep, megjelenes_eve, eredeti_nyelv, felhasznalonev, statusz) VALUES 
("86", 
"Lucy", 
"Az író-rendezõ Luc Besson a mozitörténet legkeményebb, legemlékezetesebb nõi akcióhõseit teremtette meg, elég csak a Nikitára, a Leon, a profira, vagy Az ötödik elemre gondolnunk. Besson ezúttal Scarlett Johanssont rendezi Lucy szerepében ebben az új akcióthrillerben. Lucy véletlenül egy sötét üzletbe keveredik, de szembeszáll azokkal, akik foglyul ejtették, és könyörtelen harcossá válik, aki meghaladja az emberi logikát.",
"90",
"16", 
"filmlist/86.jpg",
"2014", 
"angol", 
"admin", "1")
EOD
);



mysqli_query($connection, <<<EOD
INSERT INTO kategoriak(kategoria) VALUES ("akció"), ("dráma"), ("romantikus"), ("sci-fi"), ("thriller"), ("vígjáték");
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO film_kategoria(film_id, kategoria_id) VALUES ('9', '1'), ('9', '5'),
('10', '1'), ('11', '1'), ('12', '1'), ('13', '1'), ('14', '1'), ('15', '1'), ('16', '1'), ('17', '1'), ('18', '1'), ('19', '1'), ('20', '1'), ('21', '2'), ('22', '2'),
('23', '2'), ('24', '2'), ('25', '2'), ('26', '2'), ('27', '2'), ('28', '2'), ('29', '2'), ('30', '2'), ('31', '2'), ('32', '2'), ('33', '5'), ('34', '5'), ('35', '5'),
('36', '5'), ('37', '5'), ('38', '5'), ('39', '5'), ('40', '5'), ('41', '5'), ('42', '5'), ('43', '5'), ('44', '5'), ('45', '5'), ('46', '5'), ('47', '3'), ('48', '3'),
('49', '3'), ('50', '3'), ('51', '3'), ('52', '3'), ('53', '3'), ('54', '3'), ('55', '3'), ('56', '3'), ('57', '3'), ('58', '3'), ('59', '6'), ('60', '6'), ('61', '6'),
('62', '6'), ('63', '6'), ('64', '6'), ('65', '6'), ('66', '6'), ('67', '6'), ('68', '6'), ('69', '6'), ('70', '6'), ('71', '6'), ('72', '6'), ('73', '4'), ('74', '4'),
('75', '4'), ('76', '4'), ('77', '4'), ('78', '4'), ('79', '4'), ('80', '4'), ('81', '4'), ('82', '4'), ('83', '4'), ('84', '4'), ('85', '4'), ('86', '4');
EOD
);

mysqli_query($connection, <<<EOD
INSERT INTO kommentek(szoveg, felhasznalonev, film_id, datum) VALUES ('Király a film!', 'admin', '1', NOW()), ('Szerintetek jó volt?', 'admin', '1', NOW()), ('Láttam már jobbat is!', 'admin', '2', NOW()), ('Nem gondoltam volna, hogy tetszeni fog!', 'admin', '3', NOW()), ('Jó az oldal, jó a film!', 'admin', '4', NOW()), ('10/10!', 'admin', '5', NOW()), ('Tegnap láttam a moziban, kár volt elmenni!', 'admin', '6', NOW()), ('Első komment!', 'admin', '7', NOW()), ('Tetszetős!', 'admin', '8', NOW()), ('Jobb lett volna, ha megse nézem!', 'admin', '9', NOW()), ('Egyszerűen remek!', 'admin', '10', NOW());
EOD
);

}	
mysqli_set_charset($connection, 'utf8'); 
//Input tesztelése
function test_input($data) {
    $data = trim($data); //extra space, tab, új sor eltávolítása
    $data = stripslashes($data); // fordított perjel eltávolítása (\)
    $data = htmlspecialchars($data);// Convert special characters to HTML entities pl.: & => &amp;
    return $data;
}
?>
